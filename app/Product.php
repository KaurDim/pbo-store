<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
  protected $fillable = [
    'vendor',
    'name',
    'category_id',
    'price',
    'max_price',
    'description',
    'meta_description',
    'meta_keywords',
    'list_order'
  ];

  public function property()
  {
    return $this->hasOne(Property::class);
  }

  public function category()
  {
    return $this->belongsTo(Category::class, 'category_id');
  }

  public function collections (){
    return $this->belongsToMany(
      Collection::class,
      'product_collections',
      'product_id',
      'collection_id');
  }

  public function images()
  {
    return $this->hasMany(Image::class, 'product_id');
  }

  public function details($id)
  {
    return $this->hasMany(Details::class, 'product_id')
      ->where('order_id','=', $id)->get();
  }

  public function orders()
  {
    return $this->belongsToMany(
      Order::class,
      'order_products',
      'product_id',
      'order_id'
    );
  }

  public static function add($fields)
  {
    $product = new self;
    $product->fill($fields);
    $product->save();
    return $product;
  }

  public function setTags($id)
  {
    if ($id == null) {
      return;
    }

    $this->tags()->sync($id);
  }

  public function setInStock()
  {
    $this->in_stock = 1;
    $this->save();
  }

  public function setUnavailable()
  {
    $this->in_stock = 0;
    $this->save();
  }

  public function toggleStock($value)
  {
    if ($value) {
      return $this->setUnavailable();
    }
    return $this->setInStock();
  }


}
