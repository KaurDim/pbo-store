<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
  protected $fillable = ['name'];

  public function products()
  {
    return $this->belongsToMany(
      Product::class,
      'product_collections',
      'collection_id',
      'product_id');
  }
}
