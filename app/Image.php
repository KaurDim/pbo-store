<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  protected $fillable = ['path', 'product_id', 'material_type_id'];

  public function product()
  {
    return $this->belongsTo(Product::class);
  }

  public function material()
  {
    return $this->belongsTo(MaterialType::class, 'material_type_id');
  }

  public static function add($path, $product_id, $material_type_id)
  {
    $image = new self;
    $image->path = $path;
    $image->product_id = $product_id;
    $material_type_id ? $image->material_type_id = $material_type_id : null;
    $image->save();
    return $image;
  }
}
