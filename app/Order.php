<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['comment','customer_id', 'sum_price', 'details'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'order_products',
            'order_id',
            'product_id'
        );
    }

    public static function add($comment, $customer_id, $price, $details, $delivery, $order_id)
    {
        $order = new static;
        $order->comment = $comment;
        $order->customer_id = $customer_id;
        $order->sum_price = $price;
        $order->details = $details;
        $order->delivery = $delivery;
        $order->number = $order_id;
        $order->save();
        return $order;
    }

}
