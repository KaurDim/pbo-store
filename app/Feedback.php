<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['name','email', 'phone','comment'];

    public static function add($fields)
    {
        $feed = new static;
        $feed->fill($fields);
        $feed->save();
    }
}
