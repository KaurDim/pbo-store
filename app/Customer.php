<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name', 'email', 'phone','surname', 'address', 'city'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public static function add($field)
    {
        $customer = new static;
        $customer->fill($field);
        $customer->save();
        return $customer;
    }
}
