<?php


namespace App;


class ProductsFilter
{
  protected $builder;
  protected $request;

  public function __construct($builder, $request)
  {
    $this->builder = $builder;
    $this->request = $request;
  }


  public function apply()
  {
    foreach ($this->filters() as $filter => $value) {
      if (method_exists($this, $filter)) {
        $this->$filter($value);
      }
    }
    return $this->builder;
  }

  public function filters()
  {
    return $this->request->all();
  }

  public function category($value)
  {
    $this->builder->whereIn('category_id', $value);
  }

  public function insertions ($value) {
    $this->builder->join('properties as p', 'p.product_id' ,'=','products.id')
      ->where('insertion_id','=',$value)->select('products.*');
  }

  public function target ($value) {
    $this->builder->join('properties as p', 'p.product_id' ,'=','products.id')
      ->where('target_id','=',$value)->select('products.*');
  }

  public function collection ($value)
  {
    if (!$value) {
      return;
    }
    $this->builder->join('properties as p', 'p.product_id' ,'=','products.id')
      ->where('collection_id','=',$value)->select('products.*');
  }

  public function material($value)
  {
    $this->builder->where('material', $value);
  }

  public function has_stone($value)
  {
    $this->builder->join('properties as p', 'p.product_id' ,'=','products.id')
      ->where('has_stone','=',$value)->select('products.*');
  }
}