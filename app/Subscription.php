<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['name','email'];

    public static function add($fields)
    {
        $sub = new static;
        $sub->fill($fields);
        $sub->save();
    }
}
