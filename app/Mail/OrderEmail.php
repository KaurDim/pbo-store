<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderEmail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public $order;
  public $customer;
  public $details;

  public function __construct($order, $customer, $details)
  {
    $this->customer = $customer;
    $this->order = $order;
    $this->details = $details;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {

    $time = strtotime($this->order->created_at);
    $data = date('j M Y', $time);

    return $this->view('emails.order')
      ->with([
        'order' => $this->order,
        'customer' => $this->customer,
        'products' => $this->details,
        'createdDate' => $data
      ])
      ->subject("Спасибо за покупку (#1011)");
  }
}
