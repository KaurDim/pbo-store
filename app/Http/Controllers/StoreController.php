<?php

namespace App\Http\Controllers;

use App\Category;
use App\Collection;
use App\Customer;
use App\Image;
use App\Insertion;
use App\Material;
use App\Order;
use App\ProductsFilter;
use App\Product;
use App\Property;
use App\Stone;
use App\Target;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use Nexmo\Response;
use PhpParser\Error;

class StoreController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    try {
      $params = $request->all();
      $products = Product::with('images', 'category', 'collections')->where(
        'in_stock', '=', '1')->orderBy('list_order')->get();
      foreach ($products as $product) {
        $product->property = Property::with('insertions', 'targets')
          ->where('product_id', '=', $product->id)->first();
      }
      $categories = Category::all();
      $collections = Collection::all();
      $metals = Material::all();
      $targets = Target::all();
      $insertions = Insertion::all();

//      $products = new ProductsFilter($products, $request);
//      $products = $products->apply()->get();
//
//      if ($request->expectsJson()) {
//        return response()->json($products->toArray())
//          ->header('Vary', 'Accept');
//      }

      return view('store.store', [
        'title' => 'Магазин',
        'products' => $products,
        'categories' => $categories,
        'collections' => $collections,
        'params' => $params,
        'targets' => $targets,
        'metals' => $metals,
        'insertions' => $insertions
      ]);

    } catch (Error $error) {
      Log::error($error->getMessage());
      return abort(404);
    }
  }

  public function getProduct($id)
  {
    try {
      $product = Product::with('images', 'property', 'category', 'collections')->find($id);
      $sameProducts = Product::query()
        ->where(
          'category_id',
          '=',
          $product->category_id)
        ->whereNotIn('id', [$id])
        ->with('images', 'property', 'category')
        ->take(4)->get();

      $stones = Product::query()
        ->with('images', 'category')
        ->join('categories as c', 'c.id' ,'=','products.category_id')
        ->where('tag','=', 'gems')->select('products.*')
        ->get();
      $images = Image::query()->where('product_id', '=', $id)
        ->with('material')->get();
      $productProps = Property::query()->where('product_id', '=', $id)
        ->with('insertions', 'targets')->get();

      return view('store.product_page',
        [
          'title' => $product->name,
          'product' => $product,
          'stones' => $stones,
          'sameProducts' => $sameProducts,
          'productProps' => $productProps,
          'images' => $images,
        ]);

    } catch (\Exception $error) {
      Log::error($error->getMessage());
      return abort(404);
    }
  }

  public function order()
  {
    return view('store.order', ['title' => 'Оформить закакз']);

  }
}
