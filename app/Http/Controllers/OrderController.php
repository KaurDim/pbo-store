<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Details;
use App\Mail\OrderEmail;
use App\Mail\SubscribeEmail;
use App\Order;
use App\Subscription;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class OrderController extends Controller
{
    public function makeOrder(Request $request)
    {
      if(!$request->json()) {
        return abort(404);
      }
      $orderData = $request->json()->get('order');
      if(!$orderData || !count($orderData)) {
        return abort('404');
      }
      $user = User::query()->where('email', '=', $orderData['customer']['email'])->first();
      $customer = Customer::query()->where('email', '=', $orderData['customer']['email'])->first();
      !empty($customer) || $customer = Customer::add($orderData['customer']);

      if($orderData['subscribe']) {
        try {
          $isSubscribe = Subscription::query()->where('email', '=', $customer->email)->get();
          if(!count($isSubscribe)) {
            Subscription::add(['name' => $customer->name,'email' => $customer->email]);
          }
        } catch (\Exception $exception) {
          Log::error("Не удалось добавить в подписки $customer->email");
          Log::error($exception->getMessage());
        }
      }
      if(!empty($user)) {
        !$user->phone && $user->phone = $orderData['customer']['phone'];
        !$user->name && $user->name = $orderData['customer']['name'];
        !$user->surname && $user->surname = $orderData['customer']['surname'];
        $customer->user_id = $user->id;
        try {
          $customer->save();
          $user->save();
        } catch (\Exception $error) {
          Log::error($error->getMessage());
        }
      }
      $comment = $orderData['comment'];
      $price = $orderData['sum'];
      $delivery = $orderData['delivery'];
      $details = $orderData['detalis'];
     try {
      $date = getdate()['0'];
      if ($orderData['type'] && $orderData['type'] == 'gems') {
        $order_id = 'g-'. $date . '-' . $customer->id;
      } else {
        $order_id = 'j-'. $date . '-' . $customer->id;
      }
        $order = Order::add($comment,$customer->id, $price, $details, $delivery, $order_id);
        foreach ($orderData['products'] as $product) {
          DB::table('order_products')->insert([
            'order_id' => $order->id,
            'product_id' => $product['id']
          ]);
        }
        CartController::clearCart($orderData['type']);
        // Mail::to($customer->email)->send(new OrderEmail($order, $customer, $orderData['products']));
        return response()->json($order_id);
     } catch (\Exception $error) {
       Log::error($error->getMessage());
        return abort(500, $error->getMessage());
     }
    }

    public function orderSuccess($orderNumber) {
        $order = Order::query()->where('number', '=', $orderNumber)->first();
        return view('store.success_order', [
            'title' => 'Заказ успешно оформлен',
            'order' => $order
        ]);
    }
}
