<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Mail\FeedbackEmail;
use App\Mail\SubscribeEmail;
use App\Order;
use App\Product;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use PhpParser\Error;
use Gloudemans\Shoppingcart\Facades\Cart;


class CustomerController extends Controller
{

    public function subscribe(Request $request)
    {
        $customer = $request->json();
        $name = $customer->get('name');
        $email = $customer->get('email');
        try {
          Subscription::add($customer->all());
        } catch (\Exception $error) {
          Log::error($error->getMessage());
          return response()->json('Ошибка при подписке на рассылку!');
        }
        Mail::to($email)->send(new SubscribeEmail($name));
        return response()->json('ok');
    }

    public function sendMessage(Request $request)
    {
        $data = $request->json();
        $feedback = $data->all();
        try {
          Feedback::add($feedback);
        } catch (\Exception $error) {
          Log::error($error);
          return;
        }
        // изме
      Mail::to('kaurdim@gmail.com')->send(new FeedbackEmail($feedback));
      return response()->json('ok');
    }
}
