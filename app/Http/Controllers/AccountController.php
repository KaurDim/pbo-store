<?php

namespace App\Http\Controllers;

use App\Avatar;
use App\Customer;
use App\Order;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $currentUser = Auth::user();
        $user = User::with('avatar')
          ->where('email', '=', $currentUser->email)
          ->first();
        $customer = Customer::query()->where('user_id', '=', $user->id)->first();
        if($customer) {
          $orders = $customer->orders()->get();
          return view('account.account', ["title" => 'Аккаунт', 'user' => $user, 'orders' => $orders]);
        }
        return view('account.account', ["title" => 'Аккаунт', 'user' => $user, 'orders' => null]);
    }

    public function changeAvatar(Request $request)
    {
      $data = $request->all();
      $user = User::query()->where('email', '=', $data['email'])->first();
      if(empty($user)) {
        return;
      }
      $path = '/' . $data['avatar']->store('images/uploads/avatars');
      $avatar = Avatar::query()->where('user_id', '=', $user->id)->first();
      try {
        $avatar->path = $path;
        $avatar->save();
        return response()->json(['message' => 'Аватар успешно изменен.'], 200);
      } catch (\Exception $error) {
        return response()->json(['message' => 'Ошибка, попробуйте позже'], 500);
      }
    }

    public function updateInfo(Request $request) {
      $data = $request->json()->all();
      $user = User::query()->where('email', '=' , $data['email'])->first();
      $user->name = $data['name'];
      $user->surname = $data['surname'];
      $user->phone = $data['phone'];
      $user->save();
      return response()->json(['message' => 'Информация обновлена.', 'user' => $user], 200);
    }
}
