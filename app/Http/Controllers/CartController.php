<?php


namespace App\Http\Controllers;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;


const gems = 'gems';
const jewelry = 'jewelry';


class CartController extends Controller
{

    public function getCartData () {
    //    Cart::instance(jewelry)->destroy();
    //    Cart::instance(gems)->destroy();
        $jewelryCart = Cart::instance(jewelry)->content();
        $jewelryCount = Cart::instance(jewelry)->count();
        $jewelryTotal = Cart::instance(jewelry)->subtotal();

        $gemsCart = Cart::instance(gems)->content();
        $gemsCount = Cart::instance(gems)->count();
        $gemsTotal = Cart::instance(gems)->subtotal();
        $count = $jewelryCount + $gemsCount;
        return [
            jewelry => [
                'total' =>  $jewelryTotal,
                'cart' => $jewelryCart
            ],
            gems => [
                'total' => $gemsTotal,
                'cart' => $gemsCart
            ],
            "count" => $count
        ];
    }

    public function addToCart (Request $request)
    {
        $id = $request->json('id');
        $name = $request->json('name');
        $quantity = $request->json('quantity');
        $price = $request->json('price');
        $option = $request->json('options');
        if ($option['category']['tag'] === gems) {
            Cart::instance(gems)->add($id, $name, $quantity, $price, $option);

        } else {
            Cart::instance(jewelry)->add($id, $name, $quantity, $price, $option);
        }
        $data = $this->getCartData();
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $id = $request->json('id');
        $payload = $request->json('payload');
        Cart::instance(jewelry)->update($id, $payload);
        return response()->json($this->getCartData());
    }

    public function removeFromCart ($tag, $id)
    {
        if (!$id) {
            return response('Invalid arguments', 400);
        }
        if ($tag === gems) {
            Cart::instance(gems)->remove($id);

        } else {
            Cart::instance(jewelry)->remove($id);
        }
        return response()->json($this->getCartData());
    }

    public static function clearCart ($tag) {
        if (!$tag) {
            return response('Invalid arguments', 400);
        }
        if ($tag === gems) {
            Cart::instance(gems)->destroy();

        } else {
            Cart::instance(jewelry)->destroy();
        }
    }

    public function getCartTotal ()
    {
        $total = Cart::total();
        return response()->json($total);
    }

    public function getCartContent() {

        return response()->json($this->getCartData());
    }

    public function cart()
    {
        return view('store.cart', ['title' => 'Корзина']);

    }

    public function  cartStone() {
        return view('store.gems_cart', ['title' => 'Корзина']);
    }
}
