<?php

namespace App\Http\Controllers;

use App\Avatar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;




class AuthController extends Controller
{
    public function register(Request $request)
    {
      try {
           User::add($request->all());
       } catch(\Exception $error) {
          return response()->json(abort('202'));
       }
        Auth::attempt([
        'email' => $request->get('email'),
        'password' => $request->get('password')
        ]);
        return response()->json(true);
    }

    public function login(Request $request)
    {
      try {
        $result = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ]);
        } catch (\Exception $error) {
            return response()->json($error->getCode());
        }
      return response()->json($result);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
      try {
      $social = Socialite::driver($provider)->user();
      $id = $social->id;
      $email = $social->email;

      $user = User::with('avatar')->where('email', '=', $email)->first();
        if($user) {
          if(!$user->social_id) {
            $user->social_id = $id;
            $user->name = $social->user['given_name'];
            $user->surname = $social->user['family_name'];
            $avatar = Avatar::query()->where('user_id','=', $user->id)->first();
            $avatar->path = $social->avatar;
            $avatar->save();
            $user->save();
          }
          Auth::login($user, true);
          return redirect('/');
        }
      } catch (\Exception $error) {
        Log::error($error->getMessage());

      }
      $name = $social->user['given_name'];
      $surname = $social->user['family_name'];
      $image = $social->avatar;
      try {
        $newUser = User::socialAdd($id, $name, $surname, $email, $image);
        Auth::login($newUser, true);
        return redirect('/');
      } catch (\Exception $error) {
        Log::error($error);
        return redirect('/');
      }
    }
}
