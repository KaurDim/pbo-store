<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //Dashboard
    public function dashboard()
    {
        $products = count(Product::all());
        $customers = count(Customer::all());

        return view('admin.dashboard',['products' => $products, 'customers' => $customers]);
    }
}
