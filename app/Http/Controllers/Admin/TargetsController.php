<?php

namespace App\Http\Controllers\Admin;

use App\Target;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class TargetsController extends Controller
{
    public function index()
    {
        $targets = Target::all();
        return view('admin.targets.index', ['targets' => $targets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.targets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required', 'tag' => 'required']);
        try {
          Target::create($request->all());
          return redirect()->route('targets.index');
        } catch (\Exception $error) {
          Log::error($error->getMessage());
          throw $error;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $target = Target::find($id);
        return view('admin.targets.edit', ['target' => $target]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required']);
        $target = Target::find($id);
        $target->update($request->all());
        return redirect()->route('targets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Target::find($id)->delete();
        return redirect()->route('targets.index');
    }
}
