<?php

namespace App\Http\Controllers\admin;

use App\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class MaterialsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $materials = Material::all();
    return view('admin.materials.index', ['materials' => $materials]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.materials.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required', 'tag' => 'required']);
    try {
      Material::create($request->all());
      return redirect()->route('materials.index');
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      throw $error;
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id)
  {
    $material = Material::find($id);
    return view('admin.materials.edit', ['material' => $material]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, ['name' => 'required']);
    $material = Material::find($id);
    $material->update($request->all());
    return redirect()->route('materials.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Material::find($id)->delete();
    return redirect()->route('materials.index');
  }
}
