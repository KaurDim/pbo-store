<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $slides = DB::table('slides')->select()->get();
    return view('admin.slider.index', ['slides' => $slides]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'number' => 'required|numeric'
    ]);
    $hasSlide = DB::table('slides')->where('slide_number', '=', $request->number)->get();
    if(count($hasSlide)) {
      return abort(400, 'Слайд с таким  номером уже существует');
    }
    $images = $request->file();
    try {
      $path = $images['image']->store('images/uploads/slider');
      DB::table('slides')->insert([
        'header' => $request->header,
        'content' => $request->slideContent,
        'button_text' => $request->buttonText,
        'slide_number' => $request->number,
        'image' => $path
      ]);
      $slide = DB::table('slides')->where('slide_number','=', $request->number)->get();
      $jsonData = json_encode($slide);
      return response()->json($jsonData);
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      return abort('500');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $slide = DB::table('slides')->find($id);
    $images = $request->file();
    try {
      DB::table('slides')
        ->where('id', $id)
        ->update([
          'header' => $request->header,
          'content' => $request->slideContent,
          'button_text' => $request->buttonText,
          'slide_number' => $request->number
        ]);
      if ($images) {
        Storage::delete($slide->image);
        $path = $images['image']->store('images/uploads/slider');
        DB::table('slides')
          ->where('id', $id)
          ->update([
            'image' => $path,
          ]);
      }
      return response()->json('ok');
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      return abort(500, $error->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $slide = DB::table('slides')->find($id);
    Storage::delete($slide->image);
    DB::table('slides')->delete($id);
    $slides = DB::table('slides')->select()->get();
    return view('admin.slider.index', ['slides' => $slides]);
  }
}
