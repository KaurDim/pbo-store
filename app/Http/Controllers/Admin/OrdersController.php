<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.orders.index', ['orders' => Order::with('products')->get()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $order = Order::find($id);
      $products = $order->products;
      return view('admin.orders.edit', ['order' => Order::find($id), 'products' => $products]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $order = Order::with('customer')->find($id);
      $products = $order->products;
      return view('admin.orders.edit', ['order' => $order, 'products' => $products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->products()->detach();
        $order->delete();
        return redirect()->route('orders.index');
    }


    public function changePayments (Request $request)
    {
      $data = $request->all();

      $id = $data['orderId'];
      $payment = $data['payment'];
      if(empty($id) || empty($payment)) {
        Log::debug("Неверные праметы сохранения оплаты $id, $payment");
        return abort(404);
      }
      $order = Order::find($id);
      $order->payments = $payment;
      $order->save();

      return response()->json('ok');
    }

    public function completeOrder (Request $request)
    {
      $id = $request->json('id');
      $order = Order::with('customer')->find($id);
      $order->status = $order->completed ? 'открыт снова' : 'завершен';
      $order->completed ? $order->completed = 0 : $order->completed = 1;
      $order->save();
      return response()->json($order);
    }

    public function changeStatus (Request $request)
    {
      $id = $request->json('id');
      $status = $request->json('status');
      $order = Order::find($id);
      $order->status = $status;
      $order->save();

      return response()->json($status);
    }
}
