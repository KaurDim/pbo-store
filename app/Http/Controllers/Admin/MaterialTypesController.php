<?php

namespace App\Http\Controllers\admin;

use App\Material;
use App\MaterialType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class MaterialTypesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $materials = MaterialType::all();
    return view('admin.materialTypes.index',
      [
        'materials' => $materials,

      ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $parentMaterials = Material::all();
    return view('admin.materialTypes.create', [
      'parentMaterials' => $parentMaterials
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required|string',
      'color' => 'required',
      'material_id' => 'required|numeric'
    ]);
    MaterialType::create($request->all());
    return response()->json('ok');
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $material =  MaterialType::find($id);
    $parentMaterials = Material::all();
    return view('admin.materialTypes.edit',
      [
        'material' => $material,
        'parentMaterials' => $parentMaterials
      ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'name' => 'required|string',
      'color' => 'required',
      'material_id' => 'required|numeric'
    ]);
    $material = MaterialType::find($id);
    $material->name = $request->name;
    $material->color = $request->color;
    $material->material_id = $request->material_id;
    try {
      $material->save();
      return response()->json('ok');
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      return abort('404');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    MaterialType::find($id)->delete();
    return redirect()->route('material-types.index');
  }
}
