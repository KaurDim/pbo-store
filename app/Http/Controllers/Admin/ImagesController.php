<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    public function destroy($id)
    {
        $image = Image::find($id);
        Storage::delete($image->path);
        $image->delete();
    }
}
