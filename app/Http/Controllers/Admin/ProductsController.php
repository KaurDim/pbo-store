<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Collection;
use App\Insertion;
use App\Material;
use App\MaterialType;
use App\Product;
use App\Image;

use App\Property;
use App\Tag;
use App\Target;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $productsInSlider = DB::table('products_on_slider')->select('product_id')->get();
    $products = Product::with('category', 'images')->get();
    foreach ($products as $product) {
      foreach ($productsInSlider as $slide) {
        if($product->id === $slide->product_id) {
          $product->isOnSlider = true;
        }
      }
    }
    return view('admin.products.index',
      [
        'title' => 'Редактирование магазина',
        'products' => $products,
      ]);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $collections = Collection::all();
    $categories = Category::all();
    $targets = Target::all();
    $materials = Material::all();
    $materialTypes = MaterialType::all();
    $insertions = Insertion::all();
    return view('admin.products.create',
      [
        'title' => 'Добавление товара',
        'collections' => $collections,
        'categories' => $categories,
        'targets' => $targets,
        'metals' => $materials,
        'materials' => $materialTypes,
        'insertions' => $insertions
      ]);

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'category_id' => 'required',
      'price' => 'required|numeric'
    ]);
    $images = $request->file();
    if (!sizeof($images)) {
      return abort('422', 'Загрузите фотографии');
    }
    $productData = $request->all();

    $category = Category::query()->where('tag', '=', $productData['category_id'])->first();
    $productData['category_id'] = $category->id;
    $list_order = count(Product::all());
    try {
      $product = Product::add([
        'category_id' => $productData['category_id'],
        'vendor' => $productData['vendor'],
        'name' => $productData['name'],
        'price' => $productData['price'],
        'max_price' => $productData['maxPrice'],
        'description' => $productData['description'],
        'meta_description' => $productData['meta_description'],
        'meta_keywords' => $productData['meta_keywords'],
        'list_order' => $list_order
      ]);
      $product->collections()->attach(json_decode($productData['collection']));
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      return abort(500);
    }
    $imagesAttribute = json_decode($productData['images']);
    if ($images) {
      try {
        for ($i = 0; $i < count($images); $i++) {
          $path = $images["image$i"]->store('images/uploads/product');
          Image::add($path, $product->id, $imagesAttribute[$i]->material);
        }
      } catch (\Exception $error) {
        $product->delete();
        Log::error($error->getMessage());
        return abort(500);
      }
    }
    if ($product->category->tag !== 'gems') {
      $props = json_decode($request->get('properties'));
      try {
        $propsData = [
          'product_id' => $product->id,
          'has_stone' => $props->has_stone,
          'material_id' => $props->material,
          'diamonds' => $props->diamonds
        ];
        $property = Property::add($propsData);
        $property->insertions()->attach($props->insertions);
        $property->targets()->attach($props->targets);
      } catch (\Exception $error) {
        $images = Image::where('product_id', $product->id)->get();
        foreach ($images as $image) {
          Storage::delete($image->path);
          $image->delete();
        }
        $product->delete();
        Log::error($error->getMessage());
        return abort(500);
      }
    }
    return response()->json('ok');
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $product = Product::with('images', 'category','collections')->find($id);
    $product->property = Property::with('insertions', 'targets')
      ->where('product_id', '=', $product->id)->first();
    $collections = Collection::all();
    $categories = Category::all();
    $targets = Target::all();
    $materials = Material::all();
    $materialTypes = MaterialType::all();
    $insertions = Insertion::all();
    return view('admin.products.edit',
      [
        'product' => $product,
        'collections' => $collections,
        'categories' => $categories,
        'targets' => $targets,
        'metals' => $materials,
        'materials' => $materialTypes,
        'insertions' => $insertions
      ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'category_id' => 'required',
      'price' => 'required|numeric'
    ]);

    $product = Product::find($id);
    $product->vendor = $request->vendor;
    $product->name = $request->name;
    $product->price = $request->price;
    $product->description = $request->description;
    $product->meta_description = $request->meta_description;
    $product->meta_keywords = $request->meta_keywords;
    $product->collections()->sync(json_decode($request->collection));
    if ($product->category->tag !== 'gems') {
      $props = Property::query()->where('product_id', '=', $id)->first();
      $propsData = json_decode($request->get('properties'));
      $props->has_stone = $propsData->has_stone;
      $props->material_id = $propsData->material;
      $props->diamonds = $propsData->diamonds;
      $props->insertions()->sync(json_decode($propsData->insertions));
      $props->targets()->sync(json_decode($propsData->targets));

      $props->save();
    }
    $product->save();
    $images = $request->file();
    $imagesAttribute = json_decode($request->images);
    for ($i = 0; $i < count($imagesAttribute); $i++) {
      if (!$imagesAttribute[$i]->file && $imagesAttribute[$i]->id) {
        $img = Image::find($imagesAttribute[$i]->id);
        if ($imagesAttribute[$i]->material) {
          $img->material_type_id = $imagesAttribute[$i]->material;
        } else {
          $img->material_type_id = null;
        }
        $img->save();
      } else {
        if ($images) {
          $id = $imagesAttribute[$i]->file;
          $path = $images["image$id"]->store('images/uploads/product');
          Image::add($path, $product->id, $imagesAttribute[$i]->material);
        }
      }
    }
    return response()->json('ok');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $product = Product::find($id);
    $images = Image::where('product_id', $id)->get();
    $property = Property::where('product_id', $id)->first();
    if(count($property)) {
      $property->targets()->detach();
      $property->insertions()->detach();
      $property->delete();
    }
    foreach ($images as $image) {
      Storage::delete($image->path);
      $image->delete();
    }
    $product->collections()->detach();
    $product->delete();
    return redirect()->route('products.index');
  }

  public function deletProduct ($id)
  {
    $product = Product::find($id);
    $images = Image::where('product_id', $id)->get();
    $property = Property::where('product_id', $id)->first();
    if (count($property)) {
      $property->targets()->detach();
      $property->insertions()->detach();
      $property->delete();
    }
    foreach ($images as $image) {
      Storage::delete($image->path);
      $image->delete();
    }
    $product->collections()->detach();
    $product->delete();
    return redirect()->route('products.index');
  }

  public function deleteProductImage ($id)
  {
    $image = Image::find($id);
    Storage::delete($image->path);
    $image->delete();
    return response()->json('ok');
  }

  public function changeVisible($id)
  {
    $product = Product::find ($id);
    if ($product->in_stock) {
      $product->in_stock = 0;
    } else {
      $product->in_stock = 1;
    }
    $product->save();
    return response()->json('ok');
  }

  public function changeOrder (Request $request) {
    $orderedProducts = $request->get('orderedProducts');
    foreach ($orderedProducts as $order) {
      $product = Product::find($order['id']);
      $product->list_order = $order['list_order'];
      $product->save();
    }
    return response()->json('ok');
  }

  public function addToSlider ($id) {
    $sliderTable = DB::table('products_on_slider');
    $productInSlider = $sliderTable->where('product_id', '=', $id)->first();
    if($productInSlider) {
      $sliderTable->delete($productInSlider->id);
      return response()->json('delete');
    }
    DB::table('products_on_slider')->insert(['product_id' => $id]);
    return response()->json('add');
  }
}
