<?php

namespace App\Http\Controllers\admin;

use App\Insertion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InsertionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
    $insertions = Insertion::all();
    return view('admin.insertions.index', ['insertions' => $insertions]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.insertions.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required', 'tag' => 'required']);
    try {
      Insertion::create($request->all());
      return redirect()->route('insertions.index');
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      throw $error;
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id)
  {
    $insertion = Insertion::find($id);
    return view('admin.insertions.edit', ['insertion' => $insertion]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, ['name' => 'required']);
    $insertion = Insertion::find($id);
    $insertion->update($request->all());
    return redirect()->route('insertions.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Insertion::find($id)->delete();
    return redirect()->route('insertions.index');
  }
}
