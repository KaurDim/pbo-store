<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{

  protected $fillable = ['product_id','order_id','stones_id','size', 'material', 'gold', 'quantity'];

  public function product()
  {
    return$this->belongsTo(Product::class);
  }

  public static function add($fields)
  {
    $details = new self;
    $details->fill($fields);
    $details->save();
    return $details;
  }
}
