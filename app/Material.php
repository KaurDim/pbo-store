<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
  protected $fillable = ['name', 'tag'];

  public function properties()
  {
    return $this->hasMany(Property::class);
  }

  public function types()
  {
    return $this->hasMany(
      MaterialType::class,
      'material_id'
    );
  }


}
