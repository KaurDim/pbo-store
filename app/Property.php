<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
  protected $fillable = [
    'product_id',
    'target_id',
    'has_stone',
    'insertion_id',
    'material_id',
    'diamonds'
  ];

  public function product()
  {
    return $this->belongsTo(Product::class, 'product_id');
  }

  public function targets()
  {
    return $this->belongsToMany(
      Target::class,
      'property_targets',
      'property_id',
      'target_id');
  }

  public function material()
  {
    return $this->belongsTo(Material::class, 'material_id');
  }

  public function insertions()
  {
    return $this->belongsToMany(
      Insertion::class,
      'property_insertions',
      'property_id',
      'insertion_id');
  }

  public static function add($fields)
  {
    $prop = new self;
    $prop->fill($fields);
    $prop->save();
    return $prop;
  }

}
