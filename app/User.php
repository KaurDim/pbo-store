<?php

namespace App;

use App\Mail\ResetPwdEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
// use config\constatns\AVATAR_DEFAULT;

class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  public function avatar()
  {
    return $this->hasOne(Avatar::class);
  }

  public function customer()
  {
    return $this->hasOne(Customer::class);
  }

  public static function add($fields)
  {
    $user = new static;
    $user->fill($fields);
    $user->password = bcrypt($fields['password']);
    $user->save();
    $AVATAR_DEFAULT = 'images/avatar/default-user.png';
    Avatar::create(['path' => $AVATAR_DEFAULT, 'user_id' => $user->id]);
    return $user;
  }

  public static function socialAdd($id, $name, $surname, $email, $avatar)
  {
    $user = new static;
    $user->name = $name;
    $user->password = 'social';
    $user->surname = $surname;
    $user->email = $email;
    $user->social_id = $id;
    $user->save();
    Avatar::create(['path' => $avatar, 'user_id' => $user->id]);
    return $user;
  }

  public static function changeAvatarImage($user, $path)
  {
    $avatar = Avatar::query()->where('user_id', '=', $user->id)->first();
    try {
      $avatar->path = $path;
      $avatar->save();
      return true;
    } catch (\Exception $error) {
      Log::error($error->getMessage());
      return false;
    }
  }

  public function sendPasswordResetNotification($token)
  {
    $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
  }

}
