<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
  protected $fillable = ['name', 'tag'];

  public function properties()
  {
    return $this->belongsToMany(
      Property::class,
      'property_targets',
      'insertion_id',
      'target_id');
  }
}
