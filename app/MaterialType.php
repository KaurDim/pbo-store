<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialType extends Model
{

  protected $fillable = ['name', 'color','material_id'];

  public function images()
  {
    return $this->hasMany(Image::class, 'material_type_id');
  }

  public function material()
  {
    return $this->belongsTo(Material::class, 'material_id');
  }
}
