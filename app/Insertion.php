<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insertion extends Model
{
  protected $fillable = ['name', 'tag'];

  public function properties()
  {
    return $this->belongsToMany(
      Property::class,
      'property_insertions',
      'insertion_id',
      'property_id');
  }
}
