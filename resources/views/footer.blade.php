<footer class="footer">
  <div class="footer-nav">
    <div class="footer-link">
      <a href="/catalog">Каталоги украшений</a>
      <a href="/instructions">Полезные инструкции</a>
      <a href="/store-policy">Политика конфиденциальности</a>
      <a href="/shipping">Условия покупки, доставки и возврата</a>
    </div>
    <div class="social-link">
      <a href="https://instagram.com/pbo_rus?utm_source=ig_profile_share&igshid=uixhj57jxkqk" class="instagram-link">
        <img src="https://static.wixstatic.com/media/81af6121f84c41a5b4391d7d37fce12a.png/v1/fill/w_26,h_26,al_c,q_80,usm_1.20_1.00_0.01/81af6121f84c41a5b4391d7d37fce12a.webp">
      </a>
      <a href="#" class="facebook-link">
        <img src="https://static.wixstatic.com/media/23fd2a2be53141ed810f4d3dcdcd01fa.png/v1/fill/w_26,h_26,al_c,q_80,usm_1.20_1.00_0.01/23fd2a2be53141ed810f4d3dcdcd01fa.webp">
      </a>
      <a href="#" class="youtube-link"><i class="fab fa-youtube"></i></a>
    </div>
    <div class="subscribe-newsletter">
      <form name="subscribe-form" id="formSubscribe" method="post">
        {{ csrf_field() }}
        <label>Подпишитесь на рассылку, чтобы не пропустить все самое интересное!</label>
        <div class="subscribe">
          <input type="text" placeholder="Ваше имя" name="username" required>
          <input type="mail" placeholder="Ваш e-mail" name="email" required>
          <input type="submit" value="Подписаться" id="btnSubscribe">
        </div>
        <div id="subscribeMsg"></div>
      </form>
    </div>
    <div class="contact">
      <span>+7 (812) 925-62-45</span>
      <span>info@perborupdesign.ru</span>
    </div>
    <address class='address'>
      <span>197198 РФ, г. Санкт-Петербург, ул. Ропшинская 1/32, литер. А, пом. 4Н/24</span>
      <span>ИНН 7813264186, КПП 781301001</span>
      <span>AO “Тинькофф Банк”, г. Москва, Р/С 40702810210000053351,</span>
      <span>К/С 30101810145250000974, БИК 044525974</span >
    </address>
  </div>
  <div class="footer-signature row-container">
    <p class="signature">Sparkling Future 2019. All rights reserved.</p>
  </div>
</footer>