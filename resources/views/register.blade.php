<div id="registration" class="registration-container" style="display: none;">
    <h1>Регистрация</h1>
    <div class="row-container registration">

        <form class="container reg-form" name="registrationForm" id="registrationForm"
              method="POST" action="/register">
            @csrf
            <input type="text" placeholder="Ваше имя" id="name" name="name"  required>
            <input type="email" placeholder="Email" id="email" name="email"  required>
            <input type="password" placeholder="Пароль" id="password" name="password"  required>
            <input type="password" placeholder="Повторите пароль" id="password-confirm" name="password_confirmation" required>
            <button type="submit" class="reg-btn" >ОК</button>
            <div id="errorMsg"></div>
            <div class="enter-link link">
                Уже есть аккаунт?  <span id="signBtn">Войти</span>
            </div>
        </form>
        <div class="social-reg container">
            <a href="https://oauth.vk.com/authorize?client_id=5490057&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=5.52" class="facebook">Войти через Facebook</a>
            <a href="#" class="google">Войти через Google+</a>
        </div>
        <div class="mobile-social-container">
            <a href="#" class="facebook"><i class="fab fa-facebook-square"></i></a>
            <a href="#" class="google"><i class="fab fa-google-plus"></i></a>
        </div>
    </div>
    <div class="privacy-link link">
        Регистрируясь, вы принимаете <a href="#">политику конфиденциальности</a>
    </div>
</div>