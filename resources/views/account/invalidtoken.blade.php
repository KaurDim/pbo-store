@extends('template')

@section('content')
<div class="invalid-token-message">
  <p>К сожалению, время дейсвтия вашего запроса на восстановление пароля истекло.</p>
  <p>Пожалуйста, повторите запрос на странице авторизации.</p>
</div>
@endsection

<style>
  .invalid-token-message {
    margin:  10% auto;
    padding: 40px;
    font-family: Futura;
    font-size: 20px;
    max-width: 600px;
    border: 1px solid #242424;
    background-color: rgba(225, 106, 115, 0.1);
  }
</style>