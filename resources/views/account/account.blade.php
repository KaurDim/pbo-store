@extends('template')

@section('content')
    <account
        :user="{{json_encode($user)}}"
        :orders="{{json_encode($orders)}}"
    >
    </account>
{{--<div class="p-a-c">--}}
    {{--<div class="personal-menu">--}}
        {{--<div class="account-side-bar">--}}
            {{--<div class="user-logo">--}}
                {{--<div class="user-image">--}}
                    {{--<img src="{{$user->avatar->path}}" alt="user-logo">--}}
                {{--</div>--}}
                {{--<input class="user-name" type="text" value="{{$user->name}} {{$user->surname}}"  disabled/>--}}
            {{--</div>--}}
            {{--<button class="edit-btn">Редактировать</button>--}}
        {{--</div>--}}
        {{--<nav class="account-nav">--}}
            {{--<ul>--}}
                {{--<li><a href="#">Мои заказы</a></li>--}}
                {{--<li><a href="#">Мой профиль</a></li>--}}
            {{--</ul>--}}
        {{--</nav>--}}
    {{--</div>--}}

    {{--<div class="personal-info">--}}
        {{--<div class="account-headline">--}}
            {{--<h1>Мой аккаунт</h1>--}}
            {{--<p>Проверьте и отредактируйте личную информацию ниже.</p>--}}
        {{--</div>--}}
        {{--<div class="invariable-info">--}}
            {{--<p>Эл. почта логина:</p>--}}
            {{--<span>{{$user->email}}</span><span class="info-flag"><i class="fas fa-info-circle"></i></span>--}}
        {{--</div>--}}
        {{--<form method="post"  class="account-info-form ">--}}
            {{--@csrf--}}
            {{--<div class="row-container form-input-container">--}}
                {{--<div class="input-container">--}}
                    {{--<label for="">Имя</label>--}}
                    {{--<div><input type="text" name="name" value="{{$user->name}}"></div>--}}
                {{--</div>--}}
                {{--<div class="input-container">--}}
                    {{--<div><label for="">Фамилия</label></div>--}}
                    {{--<div><input type="text" name="surname" value="{{$user->surname}}"></div>--}}
                {{--</div>--}}
                {{--<div class="input-container">--}}
                    {{--<div><label for="">Телефон</label></div>--}}
                    {{--<div><input type="tel" name="phone" value="{{$user->phone}}"></div>--}}
                {{--</div>--}}
                {{--<input type="text" name="id" value="{{$user->id}}" hidden>--}}
            {{--</div>--}}
            {{--<button  class="update-info">Обновить</button>--}}
        {{--</form>--}}
    {{--</div>--}}
{{--</div>--}}


@endsection
