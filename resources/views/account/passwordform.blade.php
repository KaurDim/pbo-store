@extends('template')

@section('content')
  <reset-password-form
    :requestToken="{{json_encode($token)}}"
  >
  </reset-password-form>
@endsection
