@extends('admin.admin')

@section('content')
  <edit-order
    :order="{{json_encode($order)}}"
  ></edit-order>
@endsection