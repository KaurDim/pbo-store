@extends('admin.admin')

@section('content')
  <table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
      <th>id</th>
      <th>Номер</th>
      <th>Дата</th>
      <th>Сумма заказа</th>
      <th>Украшения</th>
      <th>Комментарий</th>
      <th>Статус</th>
      <th>Оплата</th>
      <th>Покупатель</th>
      <th>Редактировать</th>
    </tr>
    </thead>
    <tbody>




    @forelse($orders as $order)
      <tr>
        <td><a href="{{route('orders.edit', $order->id)}}">{{$order->id}}</a></td>
        <td>{{$order->number}}</td>
        <td>{{$order->created_at}}</td>
        <td>{{$order->sum_price}}</td>
        <td style="width: 200px">
          @foreach($order->products as $product)
            <span><a href="{{route('products.edit', $product->id)}}" >{{$product->name}}</a></span><br>
          @endforeach
        </td>
        <td>{{$order->comment}}</td>
        <td>{{$order->status}}</td>
        <td>{{$order->payment ? 'Да' : 'Нет'}}</td>
        <td>{{$order->customer->name}} {{$order->customer->surname}}</td>
        <td>
          <div class="container">
            <div class="row">
              <a href="{{route('orders.edit', $order->id)}}"><i class="fas fa-edit"></i></a>
              {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
              <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                      title="Удалить заказ"
                      class="btn  btn-sm" onclick="return   confirm('Удалить заказ?')"><i class="fas fa-trash"></i>
              </button>
              {!! Form::close() !!}
            </div>
          </div>
        </td>
      </tr>
    @empty
      <td colspan="7" class="text-center"><h3>Заказов пока нет</h3></td>
    @endforelse
    </tbody>
  </table>
@endsection