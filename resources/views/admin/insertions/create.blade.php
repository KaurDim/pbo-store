@extends('admin.admin')

@section('content')
  <div class="container">
    <h2>Добавить новые вставки</h2>
    <div class="row justify-content-start">
      <div class="col-sm-8">
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif

        {!! Form::open(['route' => 'insertions.store']) !!}
        <div class="form-group">
          <label for="collection">Уникальный иднтификтор (на английском).</label>
          <input type="text" class="form-control" id="collection" name="tag"
                 placeholder="Введите название тега" required>
        </div>
        <div class="form-group">
          <label for="collection">Заголовок для отображения на странице.</label>
          <input type="text" class="form-control" id="collection" name="name"
                 placeholder="Введите название заголовка" required>
        </div>
        @include('admin.errors')
        <button type="submit" class="btn btn-dark mb-2">Сохранить</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection