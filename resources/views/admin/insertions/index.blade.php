@extends('admin.admin')

@section('content')
    <a href="{{route('insertions.create')}}" class="btn btn-dark pull-right mb-3">Добавить вставки</a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>id</th>
            <th>Название</th>
            <th>Tag</th>
            <th>Редактировать</th>
        </tr>
        </thead>
        <tbody>
        @forelse($insertions as $insertion)
            <tr>
                <td>{{$insertion->id}}</td>
                <td>{{$insertion->name}}</td>
                <td>{{$insertion->tag}}</td>
                <td>
                    <div class="container">
                        <div class="row">
                            <a href="{{route('insertions.edit', $insertion->id)}}"><i class="fas fa-edit"></i></a>
                            {!! Form::open(['route' => ['insertions.destroy', $insertion->id], 'method' => 'delete']) !!}
                            <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                                    class="btn  btn-sm" onclick="return  confirm('Удалить вставки?')"><i class="fas fa-trash"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <td colspan="7" class="text-center"><h3>Вставок не добавлено</h3></td>
        @endforelse
        </tbody>

    </table>

@endsection