@extends('admin.admin')

@section('content')
  <div class="container">
    <h2>Изменить название тега <small>№ {{$insertion->id}}</small></h2>
    <div class="row justify-content-start">
      <div class="col-sm-8">
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        {!! Form::open(['route' => ['insertions.update', $insertion->id], 'method' => 'put']) !!}
        <div class="form-group">
          <label for="collection">Уникальный иднтификтор (на английском).</label>
          <input type="text" class="form-control" id="collection" name="tag"
                 value="{{$insertion->tag}}" required>
        </div>
        `
        <div class="form-group">
          <label for="collection">Заголовок для отображения на странице.</label>
          <input type="text" class="form-control" id="collection" name="name"
                 value="{{$insertion->name}}" required>
        </div>
        @include('admin.errors')
        <button type="submit" class="btn btn-dark mb-2">Изменить</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection