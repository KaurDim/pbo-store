@extends('admin.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h2>Добавить сменный камень</h2>
            <div class="col-sm-8">
                @if($errors->any())
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 offset-1">
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger" role="alert">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                {!! Form::open(['route' => 'stones.store', 'files' => true]) !!}
                <div class="form-group">
                    <label for="stone_name">Название камня</label>
                    <input type="text" class="form-control" id="stone_name" name="name" placeholder="Введите название камня" required>
                </div>
                <div class="form-group">
                    <label for="price">Цена</label>
                    <input type="text" class="form-control" name="price" id="price" placeholder="Цена">
                </div>

                <div class="form-group">
                    <label for="pictures">Добавьте картинку</label>
                    <input type="file" class="form-control" name="image" id="image">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Сохранить</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
