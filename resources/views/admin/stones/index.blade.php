@extends('admin.admin')

@section('content')
    <a href="{{route('stones.create')}}" class="btn btn-primary pull-right mb-3">Добавить сменный камень</a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
            <th>id</th>
            <th>Название</th>
            <th>Цена</th>
            <th>Изображение</th>
            <th>Редактировать</th>
            </tr>
        </thead>
        <tbody>
        @forelse($stones as $stone)
            <tr>
                <td>{{$stone->id}}</td>
                <td>{{$stone->name}}</td>
                <td>{{$stone->price}}</td>
                <td><img src="/{{$stone->image->path}}" class="img-fluid" style="width: 100px;"></td>
                <td>
                    <div class="container">
                        <div class="row">
                    <a href="{{route('stones.edit', $stone->id)}}"><i class="fas fa-edit"></i></a>
                    {!! Form::open(['route' => ['stones.destroy', $stone->id], 'method' => 'delete']) !!}
                    <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                            class="btn  btn-sm" onclick="return  confirm('Удалить семнный камень?')"><i class="fas fa-trash"></i></button>
                    {!! Form::close() !!}
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <div class="card">
                <div class="alert alert-info" role="alert">
                    <strong>Сменных камней не добавлено</strong>
                </div>
            </div>
        @endforelse
        </tbody>
    </table>

@endsection
