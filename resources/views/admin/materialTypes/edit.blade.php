@extends('admin.admin')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <h2>Измените тип материала {{$material->name}}</h2>
        <hr class="mb-5"/>
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        <edit-material
            :material="{{json_encode($material)}}"
            :parent="{{json_encode($parentMaterials)}}"
        >
        </edit-material>
      </div>
    </div>
  </div>
  </div>
@endsection