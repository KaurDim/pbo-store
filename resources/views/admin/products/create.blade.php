@extends('admin.admin')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <h2>Добавьте новый товар</h2>
        <hr class="mb-5"/>
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        <create-product
            :categories="{{json_encode($categories)}}"
            :collections="{{json_encode($collections)}}"
            :targets="{{json_encode($targets)}}"
            :materials="{{json_encode($materials)}}"
            :insertions="{{json_encode($insertions)}}"
            :metals="{{json_encode($metals)}}"
        >
        </create-product>
      </div>
    </div>
  </div>
@endsection