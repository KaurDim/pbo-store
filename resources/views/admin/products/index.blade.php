@extends('admin.admin')

@section('content')
  <a href="{{route('products.create')}}" class="btn btn-dark pull-right mb-3">Добавить товар</a>
  <drop-table
      :products="{{json_encode($products)}}"
  ></drop-table>
@endsection