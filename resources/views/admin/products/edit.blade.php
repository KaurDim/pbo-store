@extends('admin.admin')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <h2 class="mb-5">Редактировать {{$product->name}}</h2>
      <div class="col-sm-12">
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        <edit-product
            :product="{{json_encode($product)}}"
            :categories="{{json_encode($categories)}}"
            :collections="{{json_encode($collections)}}"
            :targets="{{json_encode($targets)}}"
            :materials="{{json_encode($materials)}}"
            :insertions="{{json_encode($insertions)}}"
            :metals="{{json_encode($metals)}}"
        >
        </edit-product>
      </div>
    </div>
  </div>
@endsection