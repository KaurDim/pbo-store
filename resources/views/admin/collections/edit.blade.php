@extends('admin.admin')

@section('content')
  <div class="container">
    <div class="row justify-content-start">
      <h2>Изменить название коллекции</h2>
      <div class="col-sm-8">
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        {!! Form::open(['route' => ['collections.update', $collection->id], 'method' => 'put']) !!}
        <div class="form-group">
          <label for="collection">Название коллеции</label>
          <input type="text" class="form-control" id="collection" name="name"
                 value="{{$collection->name}}" required>
        </div>
        @include('admin.errors')
        <button type="submit" class="btn btn-dark mb-2">Изменить</button>
        {!! Form::close() !!}

      </div>
    </div>
  </div>
@endsection