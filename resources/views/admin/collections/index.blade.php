@extends('admin.admin')

@section('content')
  <a href="{{route('collections.create')}}" class="btn btn-primary pull-right mb-3">Добавить коллекцию</a>
  <table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
      <th>id</th>
      <th>Название</th>
      <th>Редактировать</th>
    </tr>
    </thead>
    <tbody>
    @forelse($collections as $collection)
      <tr>
        <td>{{$collection->id}}</td>
        <td>{{$collection->name}}</td>
        <td>
          <div class="container">
            <div class="row">
              <a href="{{route('collections.edit', $collection->id)}}"><i class="fas fa-edit"></i></a>
              {!! Form::open(['route' => ['collections.destroy', $collection->id], 'method' => 'delete']) !!}
              <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                      class="btn  btn-sm" onclick="return  confirm('Удалить коллекцию?')"><i class="fas fa-trash"></i>
              </button>
              {!! Form::close() !!}
            </div>
          </div>

        </td>
      </tr>
    @empty
      <td colspan="7" class="text-center"><h3>Коллекций нет</h3></td>
    @endforelse
    </tbody>

  </table>

@endsection