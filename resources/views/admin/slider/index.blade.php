@extends('admin.admin')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <h2>Панель управления слайдером</h2>
        <hr class="mb-5"/>
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        @for ($i = 0; $i < count($slides); $i++)
          <create-slider
              :slide="{{json_encode($slides[$i])}}"
              :index="{{$i}}"
          >
          </create-slider>
        @endfor
        <create-slider></create-slider>
      </div>
    </div>
  </div>
@endsection