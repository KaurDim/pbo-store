@extends('admin.admin')

@section('content')
    <a href="{{route('materials.create')}}" class="btn btn-dark pull-right mb-3">Добавить материал</a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>id</th>
            <th>Название</th>
            <th>Tag</th>
            <th>Редактировать</th>
        </tr>
        </thead>
        <tbody>
        @forelse($materials as $material)
            <tr>
                <td>{{$material->id}}</td>
                <td>{{$material->name}}</td>
                <td>{{$material->tag}}</td>
                <td>
                    <div class="container">
                        <div class="row">
                            <a href="{{route('materials.edit', $material->id)}}"><i class="fas fa-edit"></i></a>
                            {!! Form::open(['route' => ['materials.destroy', $material->id], 'method' => 'delete']) !!}
                            <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                                    class="btn  btn-sm" onclick="return  confirm('Удалить материал?')"><i class="fas fa-trash"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <td colspan="7" class="text-center"><h3>Поводов не добавлено</h3></td>
        @endforelse
        </tbody>

    </table>

@endsection