<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Панель администратора</title>
  <script src="https://kit.fontawesome.com/ba1cfbc415.js"></script>
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>

<body>
  <div id="app" class='admin-template'>
    <nav class="navigation">
      <a class="navbar-brand" href="/">
        <img src="/images/logo.webp" width="80" height="40" alt="">
      </a>
      <?php
      $url = \Illuminate\Support\Facades\Route::current()->uri();
      ?>
      <ul class="navbar-nav">
        <li class="nav-item ">
          <a class="nav-link main-link {{$url === 'admin' ? 'active' : ''}}" href="{{ url('admin') }}">
            <i class="fas fa-home"></i>
            Главная
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-link {{$url === 'admin/orders' ? 'active' : ''}}" href="{{route('orders.index')}}">
            <i class="fas fa-shopping-cart"></i>
            Заказы</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-link {{$url === 'admin/products' ? 'active' : ''}}" href="{{route('products.index')}}">
            <i class="far fa-gem"></i>
            Украшения
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-link {{$url === 'admin/users' ? 'active' : ''}}" href="{{route(('users.index'))}}">
            <i class="fas fa-users"></i>
            Пользователи</a>
        </li>
        <li class="nav-item">
          <a class="nav-link main-link {{$url === 'admin/slider' ? 'active' : ''}}" href="{{route(('slider.index'))}}">
            <i class="fas fa-images"></i>
            Слайдер</a>
        </li>
        <div class="accordion" id="accordionExample">
          <div class="card sub-menu">
            <div id="headingOne sub-menu-header">
              <span class="mb-0">
                <button class="btn btn-link menu-btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Категории товаров <i class="fas fa-angle-down"></i>
                </button>
              </span>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <ul class="card-body sub-menu-list" aria-labelledby="navbarDropdownMenuLink">
                <li><a class="nav-link main-link {{$url === 'admin/collections' ? 'active' : ''}}" href="{{route('collections.index')}}">Коллекции</a></li>
                <li><a class="nav-link main-link {{$url === 'admin/categories' ? 'active' : ''}}" href="{{route('categories.index')}}">Категории</a></li>
                <li><a class="nav-link main-link {{$url === 'admin/targets' ? 'active' : ''}}" href="{{route('targets.index')}}">Повод</a></li>
                <li><a class="nav-link main-link {{$url === 'admin/materials' ? 'active' : ''}}" href="{{route('materials.index')}}">Материалы</a></li>
                <li><a class="nav-link main-link {{$url === 'admin/material-types' ? 'active' : ''}}" href="{{route('material-types.index')}}">Типы материалов</a></li>
                <li><a class="nav-link main-link {{$url === 'admin/insertions' ? 'active' : ''}}" href="{{route('insertions.index')}}">Вставки</a></li>
              </ul>
            </div>
          </div>
        </div>
      </ul>
    </nav>
    <main class='content'>
      @yield('content')
    </main>
  </div>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/admin/admin.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('#example').DataTable();
    });
  </script>

</body>

</html>