@extends('admin.admin')

@section('content')
    <a href="{{route('products.create')}}" class="btn btn-primary pull-right mb-3">Добавить пользователя</a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>id</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Email</th>
            <th>Телефон</th>
            <th>Заказы</th>
            <th>Аватар</th>
            <th>Роль</th>
            <th>Дата регистрации</th>
            <th>Редактировать</th>
        </tr>
        </thead>
        <tbody>
        @forelse($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->surname}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->phone}}</td>
                <td>{{$user->carts_id}}</td>
                <td><img src="/{{$user->avatar->path}}"  class="img-fluid" style="width: 100px;"></td>
                <td>{{$user->is_admin ? 'Админ' : 'Клиент'}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    <div class="container">
                        <div class="row">
                            {{--<a href="{{route('users.edit', $user->id)}}"><i class="fas fa-edit"></i></a>--}}
                            {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                            <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                                    class="btn  btn-sm" onclick="return  confirm('Удалить пользователя?')"><i class="fas fa-trash"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <td colspan="7" class="text-center"><h3>Нет зарегистрированных пользователей</h3></td>
        @endforelse
        </tbody>
    </table>
@endsection