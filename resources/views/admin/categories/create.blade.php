@extends('admin.admin')

@section('content')
  <div class="container">
    <h2>Добавить категорию</h2>
    <div class="row justify-content-start">
      <div class="col-sm-8">
        @if($errors->any())
          <div class="container">
            <div class="row">
              <div class="col-md-10 offset-1">
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger" role="alert">
                    {{$error}}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endif
        {!! Form::open(['route' => 'categories.store']) !!}
        <div class="form-group">
          <label for="collection">Категория (слово на английском языке, например 'ring')</label>
          <input type="text" class="form-control" id="collection" name="tag"
                 placeholder="Категория" required>
        </div>
        <div class="form-group">
          <label for="collection">Название товара в категории (например 'кольца')</label>
          <input type="text" class="form-control" id="collection" name="name"
                 placeholder="Введите название для товара" required>
        </div>
        @include('admin.errors')
        <button type="submit" class="btn btn-primary mb-2">Сохранить</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection