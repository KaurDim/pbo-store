@extends('admin.admin')

@section('content')
                <a href="{{route('categories.create')}}" class="btn btn-primary pull-right mb-3">Добавить категории</a>
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Tag</th>
                        <th>Название категории</th>
                        <th>Редактировать</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->tag}}
                            <td>{{$category->name}}</td>
                            <td>
                                <div class="container">
                                    <div class="row">
                                        <a href="{{route('categories.edit', $category->id)}}"><i class="fas fa-edit"></i></a>
                                        {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                                        <button type="submit" style="border: none; color: #1f6fb2; background-color: transparent; outline: none;"
                                                class="btn  btn-sm" onclick="return  confirm('Удалить категорию?')"><i class="fas fa-trash"></i></button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>

                            </td>
                        </tr>
                    @empty
                        <td colspan="7" class="text-center"><h3>Категорий нет</h3></td>
                    @endforelse
                    </tbody>

                </table>

@endsection