@if($errors->any())
    <div>
        @foreach($errors->all() as $error)
            <span class="badge badge-pill badge-danger">{{ $error }}</span>
        @endforeach
    </div>
@endif
