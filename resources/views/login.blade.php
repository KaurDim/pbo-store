<div id="authorization" class="authorization-container">
    <h1>Вход</h1>
    <div class="row-container authorization">
        <form class="container reg-form" method="POST" id="loginForm">
            @csrf
            <input type="email" placeholder="Email" name="email" required>
            <input type="password" placeholder="Пароль" name="password" required>
            <div class="get-password row-container">
                <div>
                    <input type="checkbox" name="remember" id="remember">
                    <label class="save-password" for="remember">Запонить меня</label>
                </div>
                <div>
                    <a href="#">Не помню пароль</a>
                </div>
            </div>
            <div id="errorMsg"></div>
            <button type="submit" class="reg-btn">Войти</button>
            <div id=""></div>
            <div class="enter-link link">
                У вас нет аккаунта? <span id="regBtn">Регистрация</span>
            </div>
        </form>
        <div class="social-reg container">
            <a href="https://oauth.vk.com/authorize?client_id=6864595&display=page&redirect_uri=http://127.0.0.1:8000&response_type=code" class="facebook">Войти через Facebook</a>
            <a href="#" class="google">Войти через Google+</a>
        </div>
        <div class="mobile-social-container">
            <a href="#" class="facebook"><i class="fab fa-facebook-square"></i></a>
            <a href="#" class="google"><i class="fab fa-google-plus"></i></a>
        </div>
    </div>
</div>