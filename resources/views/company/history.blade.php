@extends('template')

@section('content')
    <div class="container history-container">
        <div class="caption-history">
            <h1>история бренда</h1>
        </div>
        <p class="history-description">История Per Borup Design начинается с истории юноши, по имени Пер Боруп, который почти случайно нашел свою страсть к ювелирному делу. За несколько десятилетий Пер и его сын Келл, вместе с командой талантливых мастеров ювелирного дела, создали немало коллекций, каждая из которых имеет свою историю, воплотившуюся в неповторимом дизайне украшений.</p>
        <p class="history-description">До недавнего времени компания была представлена только в скандинавских странах (Дания, Швеция и Норвегия), в которых Per Borup Design сотрудничает со многими ювелирными салонами. Но с 2014 года бренд получил распространение и в других странах мира, а с 2017 у Вас появилась уникальная возможность приобрести<br> украшения бренда в России. Предлагаем Вам ознакомиться с хронологией истории бренда по годам.</p>
        <div class="history-road row-container">
            <div class="road-left">
                <div class="history-part left ">
                    <div class="history-part-description">
                        <h3 class="year">1951</h3>
                        <p>Мать Пера узнала о стажировке у ювелира, где ее 14-летний сын обнаружил в себе талант к украшениям и влюбился в тонкую и искусную работу ручного мастерства.</p>
                    </div>
                    <div class="history-picture h_p-1951"></div>
                </div>
                <div class="history-part left">
                    <div class="history-part-description">
                        <h3 class="year">1964</h3>
                        <p>По заказу Фредерика IX Пер изготовил три кольца для королевы Ингрид, принцесс Маргарит и Бенедикт, по случаю свадьбы Анны-Марии и короля Греции, Константина.</p>
                    </div>
                    <div class="history-picture h_p-1964"></div>
                </div>
                <div class="history-part left">
                    <div class="history-part-description">
                        <h3 class="year">2014</h3>
                        <p>Руководством принимается решение о распространении продукции Per Borup Design на международном рынке. За 3 года бренд PBo появился в США и Нидерландах.</p>
                    </div>
                    <div class="history-picture h_p-2014"></div>
                </div>
            </div>
            <div class="road-right">
                <div class="history-part  right">
                    <div class="history-picture h_p-1962"></div>
                    <div class="history-part-description">
                        <h3 class="year">1962</h3>
                        <p>Пер Боруп основал одноименную компанию Per Borup Design, которую впоследствии унаследовал его сын, Келл Боруп.</p>
                    </div>
                </div>
                <div class="history-part  right">
                    <div class="history-picture h_p-1979"></div>
                    <div class="history-part-description">
                        <h3 class="year">1979</h3>
                        <p>Келл перенял от отца страсть к ювелирным украшениям и ручному мастерству, получив в 1979 году статус ювелира.</p>
                    </div>
                </div>
                <div class="history-part  right">
                    <div class="history-picture h_p-2017"></div>
                    <div class="history-part-description">
                        <h3 class="year">2017</h3>
                        <p>Компания «Спарклин Фьючер» обрела статус эксклюзивного представителя бренда PBo в России.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection