@extends('template')
<? $j = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/resources/views/company/shipping.json');
    $data =  json_decode($j);
?>
@section('content')
<div class="container shipping">
    <div class='shipping-header'>
        <h1>ПОКУПКА, ДОСТАВКА
            И ВОЗВРАТ УКРАШЕНИЙ</h1>
    </div>
    <div class='shipping-rules'>
        @foreach($data as $paragraph)
        <div class='rules'>
            <h2>{{$paragraph->header}}</h2>
            <p>{{$paragraph->content}}
            </p>
        </div>
        @endforeach
    </div>
</div>
@endsection 