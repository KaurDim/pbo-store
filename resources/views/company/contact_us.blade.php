@extends('template')

@section('content')
<div class="row-container container-contact">
    <section class="section-contact">
        <div class="section-contact section-address">
            <h2>Контактные данные</h2>
            <p class="tel">+7 (911) 925-62-45</p>
            <p class="mail">info@perborupdesign.ru</p>
            <div class="social">
                <a href="#" class="instagram-link"><i class="fab fa-instagram"></i></a>
                <a href="#" class="facebook-link"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="youtube-link"><i class="fab fa-youtube"></i></a>
            </div>
        </div>
        <h2 class='form-header'>Обратная связь</h2>
        <form action="#" class="contact-form" name="FormResponse" id="formResponse">
            <div class="row-container form-input-container-contact">
                <input type="text" name="username" class="flex-1" placeholder="Имя*">
                <input type="email" name="email" class="flex-1" required placeholder="E-mail*">
            </div>
            <input type="tel" placeholder="Телефон" name="phoneNumber" id="phoneNumber">
            <textarea name="message" cols="30" rows="10" id="message" required placeholder="Сообщение*"></textarea>
            <input type="submit" value="Отправить" id="formButton">
            <div class="alert-msg" id="alertMsg"></div>
        </form>
    </section>
    <section class="partnership">
        <h2>Сотрудничество</h2>
        <p>Уважаемые коллеги, <br>

            Благодарим Вас за интерес, проявленный к компании «Спарклин Фьючер».
            Мы всегда рады новым идеям, интересным предложениям и инновационным продуктам.
        </p>
        <p>Все реализуемые нами изделия изготавливаются датскими ювелирами вручную
            в мастерской города Копенгагена. Ассортимент продукции включает украшения
            премиум класса, изготовленные из золота 750 и 585 проб, с бриллиантами и
            натуральными ювелирными камнями, а также категорию изделий из серебра 925 пробы со
            вставками из золота, драгоценных и ювелирных камней.
        </p>
        <p>
            С предложениями о сотрудничестве и при возникновении дополнительных вопросов,
            вы всегда можете написать нам на электронную почту или обратиться по телефону.
        </p>
        <p>
            С уважением и наилучшими пожеланиями,
            команда ООО «Спарклин Фьючер»
        </p>
    </section>
</div>
@endsection 