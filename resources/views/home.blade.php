@extends('template')

@section('content')
  <section class="slider">
    <div class="main-slider">
      @foreach($slider as $slide)
        <div class="slide slide{{$slide->slide_number}}" style="background-image:url({{$slide->image}});">
          <div class="slide-content">
            <h2>{{$slide->header}}</h2>
            <p>{{$slide->content}}</p>
            <a href="/store">{{$slide->button_text}}</a>
          </div>
        </div>
      @endforeach
    </div>
    <div class="dots-container"></div>
  </section>
  <section class="karusel">
    <h2>Модели месяца</h2>
    <div class="karusel-slider">
      @foreach($productsSlider as $product)
        <div class="karusel-slide">
          <a href="/store/product/{{$product->id}}">
            <div class="slide-image"
                 style="background-image:url({{$product->images->first()->path}});">
            </div>
            <h3>{{$product->name}}</h3>
          </a>
        </div>
      @endforeach
    </div>
  </section>
  {{-- <section class="instagram">
    <h2># Instagram</h2>
    <div class="insta-window">
      <iframe src="https://boykoalexander.wixsite.com/mysite-1">
      </iframe>
    </div>
  </section> --}}
@endsection