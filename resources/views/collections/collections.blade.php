@extends('template')

@section('content')
<div class="collections container">
  <nav class="company-aside-menu">
      <ul>
        <li class="company-aside-link" data-link="changeling">
          <span class="link-label">Changeling</span>
          <span class="point"></span>
        </li>
        <li class="company-aside-link" data-link="violina">
          <span class="link-label">Violina</span>
          <span class="point"></span>
        </li>
        <li class="company-aside-link" data-link="daphne">
          <span class="link-label">Daphne</span>
          <span class="point"></span>
        </li>
        <li class="company-aside-link" data-link="twigs">
          <span class="link-label">Twigs</span>
          <span class="point"></span>
        </li>
      </ul>
    </nav>
  <div class="collections-menu">
    <div class="collection-plate plate-1" style="background-image: url('/images/02_Collections/00_Menu/changeling.png')" data-link="changeling-slider"><span>Changeling</span></div>
    <div class="collection-plate plate-2" style="background-image: url('/images/02_Collections/00_Menu/violina.png')" data-link="violina-slider"><span>Violina</span></div>
    <div class="collection-plate plate-3" style="background-image: url('/images/02_Collections/00_Menu/daphne.png')" data-link="daphne-slider"><span>Daphne</span></div>
    <div class="collection-plate plate-4" style="background-image: url('/images/02_Collections/00_Menu/twigs.png')" data-link="twigs-slider"><span>Twigs</span></div>
  </div>

  <div class="collection-container changeling">
    <div class="collection-slider-container slider-changeling swiper-container">
      <div class="changeling-slider collection-slider swiper-wrapper">
        <div 
          class="slide-1 collection-slide swiper-slide" 
          style="background-image: url('/images/02_Collections/01_Changeling/1.jpg')"></div>
        <div 
          class="slide-2 collection-slide swiper-slide" 
          style="background-image: url('/images/02_Collections/01_Changeling/2.jpg')"></div>
        <div 
          class="slide-3 collection-slide swiper-slide"
          style="background-image: url('/images/02_Collections/01_Changeling/3.jpg')"></div>
      </div>
      <div class="swiper-pagination-changeling swiper-pagination-collection swiper-pagination"></div>
      <!-- Add Arrows -->
      <div class="swiper-button-collection swiper-button-next swiper-button-next-changeling">›</div>
      <div class="swiper-button-collection swiper-button-prev swiper-button-prev-changeling">‹</div>
    </div>
    <div class="specification-collection">
      <div class="specification">
        <h3>коллекция</h3>
        <h2>violina</h2>
        <p class="particular-qualities">комбинации по стилю и настроению</p>
        <div class="line"></div>
        <p class="description-collection">Словно в сказке, безупречно романтичные и удивительные украшения из коллекции Changeling меняют свой облик, воплощая
          жизнь любые фантазии.</p>
        <p class="description-collection"> Кольца увенчаны уникальной оправой, в которую Вы самостоятельно помещаете камни. Коллекция является многофункциональной, ведь кольца и подвески Changeling меняют свой внешний облик в соответствии с выбранным камнем, открывая возможность чередовать понравившиеся Вам цвета.</p>
        <p class="description-collection">Благодаря уникальному дизайну, украшения можно подбирать по настроению и стилю, сочетая их с одеждой и аксессуарами.</p>
      </div>
      <a href="/store?collection=сhangeling" class="collection-button">Посмотреть украшения</a>
    </div>
  </div>
  <div class="collection-container violina">
    <div class="collection-slider-container slider-violina swiper-container">
      <div class="violina-slider collection-slider swiper-wrapper">
        <div 
        class="slide-1 collection-slide swiper-slide" 
        style="background-image: url('/images/02_Collections/02_Violina/1.jpg')"></div>
      <div 
        class="slide-2 collection-slide swiper-slide" 
        style="background-image: url('/images/02_Collections/02_Violina/2.jpg')"></div>
      <div 
        class="slide-3 collection-slide swiper-slide"
        style="background-image: url('/images/02_Collections/02_Violina/3.jpg')"></div>
      </div>
      <div class="swiper-pagination-violina swiper-pagination-collection swiper-pagination"></div>
      <!-- Add Arrows -->
      <div class="swiper-button-collection swiper-button-next swiper-button-next-violina">›</div>
      <div class="swiper-button-collection swiper-button-prev swiper-button-prev-violina">‹</div>
    </div>
    <div class="specification-collection">
      <div class="specification">
        <h3>коллекция</h3>
        <h2>changeling</h2>
        <p class="particular-qualities">комбинации по стилю и настроению</p>
        <div class="line"></div>
        <p class="description-collection">Словно в сказке, безупречно романтичные и удивительные украшения из коллекции Changeling меняют свой облик, воплощая
          жизнь любые фантазии.</p>
        <p class="description-collection"> Кольца увенчаны уникальной оправой, в которую Вы самостоятельно помещаете камни. Коллекция является многофункциональной, ведь кольца и подвески Changeling меняют свой внешний облик в соответствии с выбранным камнем, открывая возможность чередовать понравившиеся Вам цвета.</p>
        <p class="description-collection">Благодаря уникальному дизайну, украшения можно подбирать по настроению и стилю, сочетая их с одеждой и аксессуарами.</p>
      </div>
      <a href="/store?collection=violina" class="collection-button">Посмотреть украшения</a>
    </div>
  </div>
  <div class="collection-container daphne">
    <div class="collection-slider-container slider-daphne swiper-container">
      <div class="daphne-slider collection-slider swiper-wrapper">
        <div 
        class="slide-1 collection-slide swiper-slide" 
        style="background-image: url('/images/02_Collections/03_Daphne/1.jpg')"></div>
      <div 
        class="slide-2 collection-slide swiper-slide" 
        style="background-image: url('/images/02_Collections/03_Daphne/2.jpg')"></div>
      <div 
        class="slide-3 collection-slide swiper-slide"
        style="background-image: url('/images/02_Collections/03_Daphne/3.jpg')"></div>
      </div>
      <div class="swiper-pagination-daphne swiper-pagination-collection swiper-pagination"></div>
      <!-- Add Arrows -->
      <div class="swiper-button-collection swiper-button-next swiper-button-next-daphne">›</div>
      <div class="swiper-button-collection swiper-button-prev swiper-button-prev-daphne">‹</div>
    </div>
    <div class="specification-collection">
      <div class="specification">
        <h3>коллекция</h3>
        <h2>daphne</h2>
        <p class="particular-qualities">комбинации по стилю и настроению</p>
        <div class="line"></div>
        <p class="description-collection">Словно в сказке, безупречно романтичные и удивительные украшения из коллекции Changeling меняют свой облик, воплощая
          жизнь любые фантазии.</p>
        <p class="description-collection"> Кольца увенчаны уникальной оправой, в которую Вы самостоятельно помещаете камни. Коллекция является многофункциональной, ведь кольца и подвески Changeling меняют свой внешний облик в соответствии с выбранным камнем, открывая возможность чередовать понравившиеся Вам цвета.</p>
        <p class="description-collection">Благодаря уникальному дизайну, украшения можно подбирать по настроению и стилю, сочетая их с одеждой и аксессуарами.</p>
      </div>
      <a href="/store?collection=daphne" class="collection-button">Посмотреть украшения</a>
    </div>
  </div>
  <div class="collection-container twigs">
    <div class="collection-slider-container slider-twigs swiper-container">
      <div class="twigs-slider collection-slider swiper-wrapper">
        <div 
        class="slide-1 collection-slide swiper-slide" 
        style="background-image: url('/images/02_Collections/04_Twigs/1.jpg')"></div>
      <div 
        class="slide-2 collection-slide swiper-slide" 
        style="background-image: url('/images/02_Collections/04_Twigs/2.jpg')"></div>
      <div 
        class="slide-3 collection-slide swiper-slide"
        style="background-image: url('/images/02_Collections/04_Twigs/3.jpg')"></div>
      </div>
      <div class="swiper-pagination-twigs swiper-pagination-collection swiper-pagination"></div>
      <!-- Add Arrows -->
      <div class="swiper-button-collection swiper-button-next swiper-button-next-twigs">›</div>
      <div class="swiper-button-collection swiper-button-prev swiper-button-prev-twigs">‹</div>
    </div>
    <div class="specification-collection">
      <div class="specification">
        <h3>коллекция</h3>
        <h2>twigs</h2>
        <p class="particular-qualities">комбинации по стилю и настроению</p>
        <div class="line"></div>
        <p class="description-collection">Словно в сказке, безупречно романтичные и удивительные украшения из коллекции Changeling меняют свой облик, воплощая
          жизнь любые фантазии.</p>
        <p class="description-collection"> Кольца увенчаны уникальной оправой, в которую Вы самостоятельно помещаете камни. Коллекция является многофункциональной, ведь кольца и подвески Changeling меняют свой внешний облик в соответствии с выбранным камнем, открывая возможность чередовать понравившиеся Вам цвета.</p>
        <p class="description-collection">Благодаря уникальному дизайну, украшения можно подбирать по настроению и стилю, сочетая их с одеждой и аксессуарами.</p>
      </div>
      <a href="/store?collection=twigs" class="collection-button">Посмотреть украшения</a>
    </div>
  </div>
</div>
@endsection