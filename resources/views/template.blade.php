<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <title>{{$title}}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  @if(isset($product))
  <meta property="og:title" content="{{$product->name}}">
  <meta property="og:description" content="{{$product->meta_description || 'не описания'}}">
  <meta property="og:url" content="https://perborupdesign.ru/store/product/{{$product->id}}">
  <meta property="og:image" content="https://perborupdesign.ru/{{$product->images[0]->path}}">
  <?php 
    $collet = '';
  foreach ($product->collections as $colletion) {
    $collet .=  $colletion->name . ',';
  } ?>
  <meta property="product:brand" content="{{$collet}}">
  <meta property="product:availability" content="in stock">
  <meta property="product:price:amount" content="{{$product->price}}">
  <meta property="product:price:currency" content="RUB">
  <meta property="product:retailer_item_id" content="{{$product->id}}">
  <meta property="product:google_product_category" content="Apparel & Accessories > Jewelry">
  @else
  <meta name="description" content="Описание страницы сайта." />
  @endif
  <link rel="shortcut icon" href="{{asset('/images/favico.jpg')}}" type="image/jpg">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/all.css') }}" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
  <link href="https://unpkg.com/ionicons@4.4.6/dist/css/ionicons.min.css" rel="stylesheet">
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=3fe5b0cc-b84a-46d4-bea2-b1f9a11b08d2" type="text/javascript"></script>
  <script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>
  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '527408544727013');
    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=527408544727013&ev=PageView
&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->
</head>

<body>
  <div id="app" style="height: 100%;">
    <div class="modal-window" id="modalContainer">
      <button class="close-modal" id="closeModalBtn"><img src="/images/icon/close.svg" alt="close"></button>
      <registration></registration>
    </div>
    <div class="modal-menu">
      <nav class="mobile-menu">
        @auth
        <div class="user-login-container">
          <a href="/account/my-account" class="">
            <div class="user-account-mobile">
              <div class="mobile-img-user" style="background-image: url({{\Illuminate\Support\Facades\Auth::user()->avatar->path}});">
              </div>
              <span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
            </div>
          </a>
          <a href="/logout" class="user-login mobile-logout"><br><span>Выйти</span></a>
          @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
          <a href="/admin" class="user-login mobile-logout"><br><span>Admin</span></a>
          @endif
        </div>
        @else
        <div class="user-login-container">
          <div id="openModalBtnMob" class="open-login-mob">
            <span>Войти</span>
            <svg preserveAspectRatio="xMidYMid meet" data-bbox="0 0 50 50" data-type="shape" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50" role="img" style="stroke-width: 0px;">
              <g>
                <path d="M25 48.077c-5.924 0-11.31-2.252-15.396-5.921 2.254-5.362 7.492-8.267 15.373-8.267 7.889 0 13.139 3.044 15.408 8.418-4.084 3.659-9.471 5.77-15.385 5.77m.278-35.3c4.927 0 8.611 3.812 8.611 8.878 0 5.21-3.875 9.456-8.611 9.456s-8.611-4.246-8.611-9.456c0-5.066 3.684-8.878 8.611-8.878M25 0C11.193 0 0 11.193 0 25c0 .915.056 1.816.152 2.705.032.295.091.581.133.873.085.589.173 1.176.298 1.751.073.338.169.665.256.997.135.515.273 1.027.439 1.529.114.342.243.675.37 1.01.18.476.369.945.577 1.406.149.331.308.657.472.98.225.446.463.883.714 1.313.182.312.365.619.56.922.272.423.56.832.856 1.237.207.284.41.568.629.841.325.408.671.796 1.02 1.182.22.244.432.494.662.728.405.415.833.801 1.265 1.186.173.154.329.325.507.475l.004-.011A24.886 24.886 0 0 0 25 50a24.881 24.881 0 0 0 16.069-5.861.126.126 0 0 1 .003.01c.172-.144.324-.309.49-.458.442-.392.88-.787 1.293-1.209.228-.232.437-.479.655-.72.352-.389.701-.78 1.028-1.191.218-.272.421-.556.627-.838.297-.405.587-.816.859-1.24a26.104 26.104 0 0 0 1.748-3.216c.208-.461.398-.93.579-1.406.127-.336.256-.669.369-1.012.167-.502.305-1.014.44-1.53.087-.332.183-.659.256-.996.126-.576.214-1.164.299-1.754.042-.292.101-.577.133-.872.095-.89.152-1.791.152-2.707C50 11.193 38.807 0 25 0"></path>
              </g>
            </svg>
          </div>
        </div>
        @endauth
        <ul>
          <li class="">
            <a href="/company/about_us">Компания</a>
          </li>
          <li class="">
            <a href="/collections">Коллекции</a>

          </li>
          <li class="">
            <a href="/stones">Сменные камни</a>
          </li>
          <li class="">
            <a href="/podarki">Повод</a>
          </li>
          <li class="">
            <a href="/store">Магазин</a>
          </li>
          <li class="">
            <a href="/company/contact_us">Контакты</a>
          </li>
        </ul>
      </nav>
    </div>
    @auth
    <div class='account-menu-wrapper'>
      <div class="account-menu">
        <a href="/account/my-account">Мой профиль</a>
        <a href="/logout">Выйти</a>
        @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
        <a href="/admin">Admin</a>
        @endif
      </div>
    </div>
    @endauth
    <div class="container" id="container">
      @include('header')
      <div class="content">
        @yield('content')
      </div>
      @include('footer')
      <div class="arrow-box" id="box">
        <div class="arrow-top"></div>
      </div>
    </div>
    <div class="open-chat" id="openChat">
    </div>
    {{-- боковая корзина --}}
    <div class="modal-window-basket" id="basketModal">
      <div class="bаsket" id="basket">
        <div class="basket-header">
          <div class="arrow" id="closeBtn" data-closeModal="true">
            <img src="/images/arrow.png" alt="arrow">
          </div>Ваш заказ
        </div>
        <ProductCard></ProductCard>
      </div>
    </div>
  </div>
  <script src="/js/app.js"></script>
  <script src="/js/index.js"></script>
  <script type="module" src="/js/all/converter.js"></script>
  <script type="module" src="/js/all/all.js"></script>
</body>

</html>