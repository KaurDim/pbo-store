@extends('template')

@section('content')
<div class="container stones">
    <nav class="company-aside-menu">
        <ul>
            <li class="company-aside-link" data-link="about-stones-caption">
                <span class="link-label">О сменных камнях</span>
                <span class="point"></span>
            </li>
            <li class="company-aside-link" data-link="topaz">
                <span class="link-label">Топаз Sky blue</span>
                <span class="point"></span>
            </li>
            <li class="company-aside-link" data-link="pearls">
                <span class="link-label">Таитянский жемчуг</span>
                <span class="point"></span>
            </li>
            <li class="company-aside-link" data-link="ametist">
                <span class="link-label">Аметист</span>
                <span class="point"></span>
            </li>
            <li class="company-aside-link" data-link="agate">
                <span class="link-label">Зеленый агат</span>
                <span class="point"></span>
            </li>
            <li class="company-aside-link" data-link="crystal">
                <span class="link-label">Горный хрусталь</span>
                <span class="point"></span>
            </li>
            <li class="company-aside-link" data-link="raspberry">
                <span class="link-label">Кварц Raspberry</span>
                <span class="point"></span>
            </li>
        </ul>
    </nav>
    <div class="stone-scheme">
        <img src="/images/03_Gems/stones_scheme.jpg" alt="stone-scheme">
        <div class="scheme-block scheme-block-crystal" data-link="crystal">Горный хрусталь</div>
        <div class="scheme-block scheme-block-topaz" data-link="topaz">Топаз sky blue</div>
        <div class="scheme-block scheme-block-raspberry" data-link="raspberry">Кварц raspberry</div>
        <div class="scheme-block scheme-block-agate" data-link="agate">Зеленый агат</div>
        <div class="scheme-block scheme-block-ametist" data-link="ametist">Аметист</div>
        <div class="scheme-block scheme-block-pearls" data-link="pearls">Таитянски жемчуг</div>
    </div>
    <div class="about-stones-caption reference-anchor" id="aboutStones">
        <h1>уникальные <br> сменные камни pbo</h1>
        <p>
            Одной из самых ярких отличительных особенностей украшений бренда Per Borup Design является применение в изделиях уникальной технологии сменных центральных камней.
        </p>
        <p>
            Удивительно, но Вам больше не нужно приобретать новые украшения для каждого из своих образов или при каждой смене формата мероприятия. Теперь Вы можете носить лишь одно украшение, самостоятельно меняя в нем камни. Выбирайте тот камень, который подходит под цвет Вашей одежды, под Ваше настроение, или формат встречи, которая состоится именно сегодня.
        </p>
        <p>
            У Вас несколько мероприятий в один день? Это не страшно, просто возьмите камни с собой. Одевайте прозрачный горный хрусталь с утра на работу, смените его на яркий голубой топаз перед обедом с коллегами, а для вечера с друзьями в ресторане или семейного ужина используйте нежный таитянский жемчуг.
        </p>
        <p>
            Предлагаем Вам узнать поподробнее о каждом камне из нашей коллекции.
        </p>
        <a href="#topaz" class="stone-link">Открыть мир камней PBo</a>
    </div>
    <article class="about-stones row-container topaz reference-anchor" id="topaz">
        <div class="stone-image"></div>
        <div class="stone-desription">
            <h2>топаз <br>sky blue</h2>
            <p></p>
            <p>Топаз, родом из Мозамбика, является одним из самых ярких представителей
                коллекции сменных камней PBo.

                Утонченный небесно-голубой оттенок Sky Blue
                прекрасно подходит обладательницам светлых волос,
                а в правильно подобранном комплекте с одеждой топаз
                подчеркнет броскую внешность брюнетки и станет находкой для рыжеволосой девушки.

                В форме ограненного шара этот камень будет всегда для Вас разным
                – то искриться на свету, то поглащать Ваш взгляд глубиной своего
                цвета. Все зависит от точки зрения, точнее угла, под которым Вы смотрите на шар.</p>
            <a href="/store?has_stone=1" class="stone-link">Посмотреть камни в изделиях</a>
        </div>
        <div class="stone-product-image"></div>
    </article>
    <article class="about-stones row-container pearls reference-anchor" id="pearls">
        <div class="stone-image"></div>
        <div class="stone-desription">
            <h2>Таитянский <br>жемчуг</h2>
            <p>Загадочный жемчуг с острова Таити занимает особое место в своем семействе.
                Он появляется на свет в южной части Тихого океана - идеальных водах для жизни
                устриц Pinctada margaritifera, в которых рождаются восхитительные камни.

                Чтобы получить только одну жемчужину, требуется в около трех лет. При этом
                только в 60% устриц вырастают эти завораживающие драгоценности.

                Среди множества оттенков и форм, мастера PBo выбрали для украшений жемчуг
                дымчато-серого цвета и идеально круглой формы, наиболее ценной в мире жемчуга.</p>
            <a href="/store?has_stone=1" class="stone-link">Посмотреть камни в изделиях</a>
        </div>
        <div class="stone-product-image"></div>
    </article>
    <article class="about-stones row-container ametist reference-anchor" id="ametist">
        <div class="stone-image"></div>
        <div class="stone-desription">
            <h2>Аметист</h2>
            <p>Из множества видов камней кварцевой группы в коллекцию бренда попали
                лишь немногие ее представители, и самый яркий из них Аметист. Взгляните
                на этот глубокий цвет с винным оттенком, не зря в греческой мифологии
                камень ассоциируется с богом вина, Дионисом. В природе минерал чаще
                встречается в виде друз и аккуратных щеток, но для того, чтобы подчеркнуть
                красоту его цвета, PBo использует в украшениях камни в двух исполнениях -
                шар с огранкой, заставляющей камень переливаться разными оттенками фиолетового,
                и шар с гладкой поверхностью, раскрывающей всю полноту цвета.

                Предлагаем Вам нажать на кнопку ниже и полюбоваться красотой такого
                удивительного камня в украшениях PBo.</p>
            <a href="/store?has_stone=1" class="stone-link">Посмотреть камень в изделиях</a>
        </div>
        <div class="stone-product-image"></div>
    </article>
    <article class="about-stones row-container agate reference-anchor" id="agate">
        <div class="stone-image"></div>
        <div class="stone-desription">
            <h2>Зеленый<br>агат</h2>
            <p>Следущий камень коллекции PBo можно назвать поистине императорским, ведь он входил в
                коллекцию Екатерины Великой, а знаменитый Фаберже изготовил с его применением многие
                из своих знаменитых работ. Камень носит величественное название «Агат», имющее греческие
                корни, а на русский переводится как «счастливый».

                Оттенки камня варьируются от тонов зелени, распускающейся весной под лучами солнца,
                до темного хвойного леса в непогоду. В необработанном виде поверхность минерала матовая,
                а после полировки и огранки она приобретает стеклянный блеск. Именно таким блеском и
                палитрой зеленых оттенков обладает агат PBo.

            </p>
            <a href="/store?has_stone=1" class="stone-link">Посмотреть камни в изделиях</a>
        </div>
        <div class="stone-product-image"></div>
    </article>
    <article class="about-stones row-container crystal reference-anchor" id="crystal">
        <div class="stone-image"></div>
        <div class="stone-desription">
            <h2>Горный<br>хрусталь</h2>
            <p>Украшения Changeling могут быть как яркими и заметными, так и почти невидимыми,
                окружая Вас ореолом загадочности. Только что Ваше изделие было легким и воздушным,
                как бы пустым, но вот Вы обернулись, и в его центре заискрился камень.
                Все это заслуга удивительного, прозрачного как слеза, горного хрусталя.

                В природе кристаллы минерала чаще образуются в виде неправильного шестигранника с
                заостренной вершиной. А в украшениях PBo камень приобрел не менее интересную форму
                органенного шара.

                Хрусталь пленит взоры и чувства людей своей естесственностью. Упоминания о
                нем встречаются в «Одиссее» и «Иллиаде» Гомера. Особенно ценил горный хрусталь
                великий Карл Фаберже.</p>
            <a href="/store?has_stone=1" class="stone-link">Посмотреть камни в изделиях</a>
        </div>
        <div class="stone-product-image"></div>
    </article>
    <article class="about-stones row-container raspberry reference-anchor" id="raspberry">
        <div class="stone-image"></div>
        <div class="stone-desription">
            <h2>Кварц<br>Raspberry</h2>
            <p>Яркий, сочный и пленительный цвет этого камня очарует вас. Raspberry quarts, или в
                переводе на русский «малиновый кварц» можно использовать во всех украшениях коллекции
                Changeling, составляя уникальные комплекты.

                Кварц дарит нам большое количество разновидностей и многообразие цветов. Этим он и
                удивителен. Включение различных примесей изменяет его цвет и прозрачность. Цвет кварца
                raspberry варьируется от непрозрачно малинового до бордового.

                Одевая украшения с этим камнем нужно быть готовой к тому, что на вас обратят внимание.
                Вы станете центром притяжения на любом мероприятии, а вашему образу подарят массу комплиментов.</p>
            <a href="/store?has_stone=1" class="stone-link">Посмотреть камни в изделиях</a>
        </div>
        <div class="stone-product-image"></div>
    </article>
</div>
@endsection