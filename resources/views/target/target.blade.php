@extends('template')

@section('content')
<section class='target_container'>
  <nav class="company-aside-menu">
    <ul>
      <li class="company-aside-link" data-link="married">
        <span class="link-label">Предложение</span>
        <span class="point"></span>
      </li>
      <li class="company-aside-link" data-link="wedding">
        <span class="link-label">Свадьба</span>
        <span class="point"></span>
      </li>
      <li class="company-aside-link" data-link="gift_women">
        <span class="link-label">Подарок женщине</span>
        <span class="point"></span>
      </li>
      <li class="company-aside-link" data-link="gift_man">
        <span class="link-label">Подарок мужчине</span>
        <span class="point"></span>
      </li>
    </ul>
  </nav>
  <div class="target">
    <div class="section married" id="married">
      <div class="image">
        <h2 class='mobile_caption'>Предложение</h2>
      </div>
      <div class="slider_box">
        <h2>Предложение</h2>
        <div class="target_slider married_slider">
          <div class="slide slide_1">
            <div class="slide_content">
              <p class="slide_text">
                Основной и наиболее важной целью компании является помощь мужчине в незабываемом и
                безотказном предложении руки и сердца своей избраннице. Именно эта цель послужила
                поводом для знакомства основателя компании "Спарклин Фьючер" и бренда Per Borup Design.
              </p>
              <p class="slide_text">
                Нельзя переоценить важность этой церемонии, ведь с нее начинается семья.
                А наиболее подходящими для такого события являются кольца Changeling с технологией
                сменных камней, и вот почему (листайте вправо):
              </p>
            </div>
          </div>
          <div class="slide slide_2">
            <div class="slide_content">
              <p class="slide_text">
                Перед тем, как сделать предложение, Вы (мужчина) конечно уже
              </p>
              <p class="slide_text">
                знаете предпочтения своей половинки - ее любимые цвета, одежду и аксессуары.
              </p>
              <p class="slide_text">
                В комплекте с украшением Вы получаете от 1-го до 3-х камней на выбор, и в назначенный день,
                преклонив колено, Вы одеваете на палец невесты кольцо именно с тем камнем, который лучше
                всего подходит ей в этот знаменательный момент. Остальные же камни станут необходимым
                дополнением на самой свадьбе и других мероприятиях.
              </p>
            </div>
          </div>

        </div>
        {{--<div class="dots_container"></div>--}}
        <div class="button-box">
          <a href="/store?target=engagement">
            Хочу сделать предложение
          </a>
        </div>
      </div>
    </div>
    <div class="section wedding" id="wedding">
      <div class="image">
        <h2 class='mobile_caption'>Свадьба</h2>
      </div>
      <div class="slider_box">
        <h2>Свадьба</h2>
        <div class="target_slider wedding_slider">
          <div class="slide slide_1">
            <div class="slide_content">
              <p class="slide_text">
                Для свадебной церемонии Вам подойдут более традиционные
                обручальные кольца от PBo. Их можно подобрать
                из тех же коллекций, к которым относится и помолвочное
                кольцо со сменными камнями.
              </p>
              <p class="slide_text">
                Вместе они будут составлять семейную ценность,
                передаваемую из поколения в поколение.
              </p>
              <p class="slide_text">
                Как показала история, дизайн украшений бренда
                остаётся актуальным на долгие года. Чтобы узнать
                об этом подробнее, прочтите удивительную историю кольца Daphne.
              </p>
            </div>
          </div>
          <div class="slide slide_2">
            <div class="slide_content">
              <p class="slide_text">
                Миссия же кольца для помолвки, при выборе украшения Changeling, также
                не заканчивается после долгожданного "Да". Во время свадебной церемонии
                Вы можете заменить в нем камень на подходящий под свадебный наряд.
              </p>
              <p class="slide_text">
                Для традиционного белого платья идеально подойдут пресноводный
                жемчуг или снежный кварц. Если же платье не белое, или Вы хотите
                сочетать украшение с другими элементами Вашего стиля,
                то и это не проблема - камни можно подобрать под любой цвет и образ.
              </p>
            </div>
          </div>
        </div>
        {{--<div class="dots_container"></div>--}}
        <div class="button-box">
          <a href="/store?target=wedding">
            Планируем пожениться
          </a>
        </div>
      </div>
    </div>
    <div class="section gift_women" id="gift_women">
      <div class="image">
        <h2 class='mobile_caption'>Подарок женщине</h2>
      </div>
      <div class="slider_box">
        <div class='color'></div>
        <h2>Подарок женщине</h2>
        <div class="target_slider gift_women_slider">
          <div class="slide slide_1">
            <div class="slide_content">
              <p class="slide_text">
                Для подарков любимым - девушке, жене, матери, дочери, сестре, родственнице или подруге,
                - существует множество поводов.
                Здесь же речь пойдет не о самих поводах, а об особенностях украшений PBo предоставлять их.
                Первая и главная особенность - уникальная технология, которая избавит Вас от мук выбора.
                Однажды подарив украшение, на очередной праздник Вам теперь достаточно дополнить его
                новым камнем. У такого решения несколько преимуществ.
              </p>
            </div>
          </div>
          <div class="slide slide_2">
            <div class="slide_content">
              <p class="slide_text">
                Во-первых, Вы экономите время на выбор, точно зная, что подарок понравится и будет полезен.
              </p>
              <p class="slide_text">
                Во-вторых, покупка обойдется гораздо дешевле самого изделия, при этом одно
                и то же украшение с каждым сменным камнем выглядит как совершенно новое,
                а уже имеющиеся в коллекции камни можно использовать для других изделий,
                без труда самостоятельно переставляя их из кольца в подвеску, серьги,
                браслет или другое кольцо.
              </p>
            </div>
          </div>
          <div class="slide slide_3">
            <div class="slide_content">
              <p class="slide_text">
                Другая особенность изделий PBo предоставлять поводы для подарков -
                возможность составить комплект украшений одной серии.
              </p>
              <p class="slide_text">
                С одной стороны, практически к каждой модели можно подобрать
                комплект из той же коллекции с аналогичным дизайном (к кольцу
                - серьги, к подвеске - браслет и так далее).

              </p>
              <p class="slide_text">
                С другой стороны, Вы еще не скоро увидите подобные украшения
                на ком-либо ещё, ведь выбор моделей ограничен, изделия
                изготавливаются персонально для каждого клиента вручную, а
                сам бренд только недавно представил свой ассортимент в
                России, начав с 4-х коллекций и 6-ти сменных камней.
              </p>
            </div>
          </div>
          <div class="slide slide_4">
            <div class="slide_content">
              <p class="slide_text">
                Третья особенность - у каждой коллекции и даже у некоторых
                отдельных моделей есть собственная неповторимая история создания
                и муза, вдохновившая мастеров PBo на создание уникального дизайна.
              </p>
              <p class="slide_text">
                Например, приверженцам истории, легенд и мифов подойдут
                украшения коллекции Daphne, а любителям природы - изделия Twigs.
              </p>
              <p class="slide_text">
                Ценителям вина может понравиться украшение с аметистом,
                а предпочитающим дворцовый стиль в интерьере - с лазуритом или агатом.
              </p>
              <p class="slide_text">
                Почему это так, какие изделия по какому случаю лучше
                дарить, Вы подробно можете узнать в нашем магазине, нажав на кнопку ниже.
              </p>
            </div>
          </div>
        </div>
        {{--<div class="dots_container"></div>--}}
        <div class="button-box">
          <a href="/store?target=for woman">
            Хочу сделать подарок
          </a>
        </div>
      </div>
    </div>
    <div class="section gift_man" id="gift_man">
      <div class="image">
        <h2 class='mobile_caption'>Подарок мужчине</h2>
      </div>
      <div class="slider_box">
        <h2>Подарок мужчине</h2>
        <div class="target_slider gift_man_slider">
          <div class="slide slide_2">
            <div class="slide_content">
              <p class="slide_text">
                Сегмент мужских изделий в PBo только начинает формироваться
                и гораздо менее разнообразен в сравнении с украшениями для женщин.
                Однако и мужскую половину есть чем порадовать - запонки,
                цепочки с подвесками, браслеты. Ассортимент будет
                постоянно расширяться, следите за обновлениями!
              </p>
            </div>
          </div>
          <div class="slide slide_1">
            <div class="slide_content">
              <p class="slide_text">
                Кроме того, специально для Вас мы можем изготовить украшение
                индивидуального дизайна и в единичном экземпляре. Будучи
                обладателем уникального украшения, Вы подчеркнете свой
                статус, особый вкус и всегда будете выделяться на фоне
                окружающих. Само же изделие будет вдохновлять Вас на создание
                шедевров в творческой деятельности.
              </p>
            </div>
          </div>
        </div>
        {{--<div class="dots_container"></div>--}}
        <div class="button-box">
          <a href="/store?target=for man">
            Хочу сделать подарок
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection