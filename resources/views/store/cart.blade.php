@extends('template')

@section('content')
  <?php $stones=\App\Product::query()
    ->with('images', 'category')
    ->join('categories as c', 'c.id' ,'=','products.category_id')
    ->where('tag','=', 'gems')->select('products.*')
    ->get();   ?>
    <cart
        :stones="{{json_encode($stones)}}"
    ></cart>
@endsection