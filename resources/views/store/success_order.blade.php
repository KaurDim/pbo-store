@extends('template')
@section('content')
    <div class="success-order-container">
        <h2>Благодарим за оформление заказа!</h2>
        <p class="order-instructions">Мы свяжемся с Вами в ближайшее время для согласования всех
            деталей и назначим встречу в нашем офисе для показа и примерки образцов украшений в удобное для Вас время.
        </p>
        <div class="order-info order-block">
            <p class="order-info-part">
                <span  class="label">Номер заказа:</span>
                <span>{{$order->number}}</span>
            </p>
            <p class="order-info-part">
                <span class="label">Стоимость заказа:</span>
                <span >{{$order->sum_price}},00 руб.</span>
            </p>
        </div>
        <div class="order-block">
            <h3>Оплата:</h3>
            <p class="important">Стоимость заказа оплачивается при получении.</p>
            <p>После размещения заказа мы свяжемся с Вами для согласования всех деталей и назначим встречу в офисе для показа и примерки образцов украшений в удобное для Вас время.</p>
            <?php
            $pattern = '/g-/';
            $mrh_login = "perborupdesign";
            $mrh_pass1 = "F1HZMkI0wX11E2BdUWim";
            $inv_id = $order->number;
            $inv_desc = "Покупка украшений";
            $out_summ = $order->sum_price;
            $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
            ?>
            {{-- @if(preg_match($pattern, $order->number))
                <iframe target="_blank" class="payment_button" allowtransparency="true"
                    src="https://auth.robokassa.ru/Merchant/PaymentForm/FormMS.if?MerchantLogin={{$mrh_login}}&amp;OutSum={{$out_summ}}&amp;InvId={{$inv_id}}&amp;Description={{$inv_desc}}&amp;SignatureValue={{$crc}}">
                </iframe>
            @endif --}}
        </div>
        <div class="order-block">
            <h3>Способ получения заказа:</h3>
            <p>В офисе по адресу: г. Санкт-Петербург, ул. Невский проспект, дом 55, бизнес-центр «Невский Плаза»</p>
            <p>После подтверждения заказа дождитесь уведомления о поступлении украшения в наш офис. Далее мы согласуем с Вами дату и время получения.</p>
            <p>Срок изготовления и доставки заказа в офис - от 30 до 60 календарных дней с момента подтверждения заказа.</p>
        </div>
        <div class="return-store"><a href="/store">Вернуться в магазин</a></div>
    </div>
@endsection
