@extends('template')

@section('content')
    <product-page
        :product="{{json_encode($product)}}"
        :stones="{{json_encode($stones)}}"
        :sameproducts="{{json_encode($sameProducts)}}"
        :images = "{{json_encode($images)}}"
        :property = "{{json_encode($productProps)}}"
    >
    </product-page>
@endsection