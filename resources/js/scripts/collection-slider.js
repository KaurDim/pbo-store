import Swiper from 'swiper';

const collectionSliders = document.querySelectorAll('.collection-slider-container');

[...collectionSliders].forEach(slider => {
  const regExp = /^slider-/;
  const targetClass = [...slider.classList].find(className => {
    return regExp.test(className);
  });
  if(targetClass) {
    const collectionName = targetClass.replace(regExp, '');
    new Swiper(`.${targetClass}`, {
      speed: 600,
      autoplay: {
        delay: 5000,
      },
      loop: true,
      pagination: {
        el: `.swiper-pagination-${collectionName}`,
        clickable: true,
      },
      navigation: {
        nextEl: `.swiper-button-next-${collectionName}`,
        prevEl: `.swiper-button-prev-${collectionName}`,
      },
    });
  }

});

// const collectionSlider = new Swiper('.slider-violina', {
//   speed: 600,
//   autoplay: {
//     delay: 5000,
//   },
//   loop: true,
//   pagination: {
//     el: '.swiper-pagination-violina ',
//     clickable: true,
//   },
//   navigation: {
//     nextEl: '.swiper-button-next-violina',
//     prevEl: '.swiper-button-prev-violina',
//   },
// });

// const collectionSlider1 = new Swiper('.slider-changeling', {
//   speed: 600,
//   autoplay: {
//     delay: 5000,
//   },
//   loop: true,
//   pagination: {
//     el: '.swiper-pagination-changeling',
//     clickable: true,
//   },
//   navigation: {
//     nextEl: '.swiper-button-next-changeling',
//     prevEl: '.swiper-button-prev-changeling',
//   },
// });