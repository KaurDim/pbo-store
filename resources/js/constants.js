export const STONE_PRICE = 3900;

export const categoryConstants = {
  GEM_CATEGORY: 'gems',
  RING_CATEGORY: 'rings'
}


export const CART_TYPES = {
  jewelry: 'jewelry',
  gems: 'gems'
};