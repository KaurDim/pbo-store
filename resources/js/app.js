import ProductZoomer from 'vue-product-zoomer';
import CxltToastr from 'cxlt-vue2-toastr';
import CKEditor from '@ckeditor/ckeditor5-vue';
import VTooltip from 'v-tooltip';
import axios from 'axios';
import { CART_TYPES } from './constants';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Vuex = require('vuex');
const VueInputMask = require('vue-inputmask').default



Vue.component('productcard', require('./components/ProductCard.vue').default);
Vue.component('cart', require('./components/Cart/Cart.vue').default);
Vue.component('gems-cart', require('./components/Cart/GemsCart').default);
Vue.component('count', require('./components/CountBasket.vue').default);
Vue.component('filter-product', require('./components/Filter.vue').default);
Vue.component('order-form', require('./components/OrderForm.vue').default);
Vue.component('product-page', require('./components/ProductPage.vue').default);
Vue.component('instagram', require('./components/Instagram.vue').default);
Vue.component('gallery', require('./components/ProductGallery.vue').default);
Vue.component('slider-mobile', require('./components/sliderMobile.vue').default);
Vue.component('similar-goods', require('./components/SimilarGoods.vue').default);
Vue.component('registration', require('./components/Auth/Auth.vue').default);
Vue.component('catalog-viewer', require('./components/CatalogViewer.vue').default);
Vue.component('loader', require('./components/Loader.vue').default);
Vue.component('pre-loader', require('./components/PreLoader.vue').default);
Vue.component('instructions', require('./components/InstructionsView.vue').default);

Vue.component('create-product', require('./components/admin/product/CreateProduct.vue').default);
Vue.component('edit-product', require('./components/admin/product/EditProduct.vue').default);
Vue.component('create-material', require('./components/admin/material/CreateMaterial.vue').default);
Vue.component('edit-material', require('./components/admin/material/EditMaterial.vue').default);
Vue.component('create-slider', require('./components/admin/slider/CreateSlider.vue').default);
Vue.component('drop-table', require('./components/admin/common/DropTable.vue').default);
Vue.component('edit-order', require('./components/admin/order/EditOrder').default);
Vue.component('reset-password-form', require('./components/Auth/ResetPasswordForm').default);
Vue.component('account', require('./components/Account/Account').default);


Vue.use(Vuex);
Vue.use(ProductZoomer);
Vue.use(VueInputMask);
Vue.use(CKEditor);
Vue.use(VTooltip);

const toastrConfigs = {
    position: 'top right',
    showDuration: 2000,
    timeOut: 10000
}
Vue.use(CxltToastr, toastrConfigs);

const store = new Vuex.Store({
    state: {
        count: 0,
        cart: [],
        jewelryCart: [],
        gemsCart: [],
        total: 0
    },
    mutations: {
        update(state) {
            state.count = JSON.parse(localStorage.getItem("cart"));
        },
        updateCart(state, payload) {
            const jewelry = payload[CART_TYPES.jewelry];
            const gems = payload[CART_TYPES.gems];

            const jewelryCart = Object.keys(jewelry.cart).map(rowId => {
                return jewelry.cart[rowId];
            });
            const gemsCart = Object.keys(gems.cart).map(rowId => {
                return gems.cart[rowId];
            });
            state.jewelryCart = jewelryCart; 
            state.gemsCart = gemsCart;
            state.count = payload.count;
            let jewelryTotal = Number(jewelry.total.replace(/\s/g, ''));
            let gemsTotal = Number(gems.total.replace(/\s/g, ''));
            state.total = jewelryTotal + gemsTotal;
        },
        addToCart(state, payload) {
            state.cart.push(payload.product);
        },
        removeFromCart(state, payload) {
            state.cart = state.cart.filter(product => {
                return product.rowId !== payload.id;
            });
        }
    },
    actions: {
        update({ commit }) {
            commit('update');
        },
        updateCart({ commit }, payload) {
            commit('updateCart', payload);
        },
        addToCart({ commit }, payload) {
            commit('addToCart', payload);
        },
        removeFromCart() {
            commit('removeFromCart', payload);
        }
    }
});

new Vue({
    el: '#app',
    store,
    mounted: async function () {
        try {
            const { data } = await axios.get('/get-cart-content');
            store.dispatch('updateCart', data);
        } catch (error) {
            console.log(error);
            return;
        }
    },
});

