(function() {
    "use strict";

    let loginForm = new Form('loginForm');


    loginForm.init(
        function() {

            let email = this.form.email.value;
            let password = this.form.password.value;
            let errorMessage = null;
            let errorMsg = document.getElementById("errorMsg");


            if ( email === '' ) {
                this.form.email.classList.add("empty");
                errorMessage = "Пожалуйста, укажите действительный e`mail.";
                this.message(errorMessage, errorMsg);
                return;
            } else {this.form.email.classList.remove("empty");}
            if ( password === '') {
                this.form.password.classList.add("empty");
                errorMessage = "Укажите ваш пароль!";
                this.message(errorMessage, errorMsg);
                return;
            } else {this.form.password.classList.remove("empty");}


            let formData = {
                email: email,
                password: password

            };
            this.success(formData);
            errorMessage = null;
            console.log(formData);
            // window.location = ;
            // this.form.reset();
            return formData;
        },

        function(formData) {
            axios.post('/login', {
                email: formData.email,
                password: formData.password
            }).then( response => {
                if(response.data) {
                    window.location = '/';
                } else {
                    this.message("Не верные почта или пароль.", errorMsg);
                }
            }).catch( error => {
                    this.message("Не верные почта или пароль.", errorMsg);
                })
        },
        function() {
            this.message("Ошибка", subscribeMsg);
        }
    );
}());
