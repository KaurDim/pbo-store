$(document).ready(function(){
  
    $('.target_slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        // autoplay: true,
        cssEase: 'ease',
        autoplaySpeed: 4000,
        speed: 800,
        appendDots: $(".dots_container"),
        prevArrow: '<button type="button" class="prewBtn"><span>&#8249;</span></button>',
        nextArrow: '<button type="button" class="nextBtn"><span>&#8250;</span></button>'
    });
});
	