"use strict";


// let formButton = document.getElementById("formButton");
// let btnSubscribe = document.getElementById("btnSubscribe");

// formButton.addEventListener("click", getForm);
// btnSubscribe.addEventListener("click", getForm);


let formContact = new Form("formResponse");




formContact.init(
	function() {
		let name = this.form.username.value;
		let email = this.form.email.value;
		let phoneNumber = this.form.phoneNumber.value;
		let message = this.form.message.value;
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
		let errorMesage = null;
		let alertMsg = document.getElementById("alertMsg");
		// let pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if(!name || name.length < 3 || this.isEmptyStr(name)) {
			this.form.username.classList.add("empty");
			errorMesage = "Укажите ваше имя.";
			this.message(errorMesage, alertMsg);
			return;
		} 
		else {this.form.username.classList.remove("empty");}
		if (!email) {
			this.form.email.classList.add("empty");
			errorMesage = "Пожалуйста, укажите действительный email.";
			this.message(errorMesage, alertMsg);
			return;
		} else {this.form.email.classList.remove("empty");}
		if (!message || message.length < 5 || this.isEmptyStr(message)) {
			this.form.message.classList.add("empty");
			errorMesage = "Напишите сообщение!";
			this.message(errorMesage, alertMsg);
			return;
		} else {this.form.message.classList.remove("empty");}


        let formData = {
            name: name.trim(),
            email: email,
            phone: phoneNumber,
            comment: message.trim()
        };
        this.success(formData, token);
        errorMesage = null;
        console.log(formData);
        this.form.reset();
        return formData;
    },

    function(formData, token) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', '/feedback');
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.send(JSON.stringify(formData));
        xhr.onload = (oEvent) => {
            if (xhr.status == 200) {
                this.message("Сообщение успешно отправлено!", alertMsg);
            } else {

                this.message("Возникли проблемы, попробуйте еще раз.", alertMsg);
            }
        };

    },


function() {
	this.message("Ошибка", alertMsg);

	// this.enabled = true;
}



	


);


$(function(){
  $("#phoneNumber").mask("+7(999) 999-9999");
});