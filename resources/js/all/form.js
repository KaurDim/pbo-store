class Form {
	constructor(idForm) {
		this.form = document.getElementById(idForm);
	}

	init(submit, success, error) {
		this.submit = submit;
		this.success = success;
		this.error = error;


		this.enabled = true;

		let _this = this, listener = function() {


		let evt = arguments[0] || window.event;
		evt.preventDefault ? evt.preventDefault() : evt.returnValue = false;
		
		_this.process();

		 };

		 if(this.form)
		   this.form.addEventListener('submit', listener, false);
	}

	process() {
	  if(this.enabled) { 
	    this.enabled = false; 
	    this.submit(); 
	  }
	}

	message(text, textContent) {
		textContent.innerHTML = text;
	    this.enabled = true; 

	}
	isEmptyStr(str) {
	if(str == "") return true;
	let count = 0;
	for(let i = 0; i < str.length; ++i) {
		if(str.charAt(i) == " ") ++count;
	}
	return count == str.length;
	}
};

