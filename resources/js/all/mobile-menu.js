
(function () {

  function menuMove(modal, menu) {
    if (!this.classList.contains('open')) {
      this.classList.add('open');
      menu.parentNode.classList.add('visibility');
      setTimeout(function () {
        menu.classList.add('show');
      }, 100)

    } else if (this.classList.contains('open')) {
      this.classList.remove('open');
      menu.classList.remove('show');
      setTimeout(function () {
        menu.parentNode.classList.remove('visibility');
      }, 300)
    } else {
      return;
    }
  }

  function menuClose(menu, btn, event) {
    if (event.target === menu) {
      return;
    } else {
      btn.classList.remove('open');
      menu.classList.remove('show');
      setTimeout(function () {
        this.classList.remove('visibility');
      }.bind(this), 400)
    }
  }
  let mobMenu = document.querySelector('nav.mobile-menu');
  let modalContainer = mobMenu.parentNode;
  const menuBtn = document.querySelector('.wrapper-menu');

  menuBtn.addEventListener('click', menuMove.bind(menuBtn, modalContainer, mobMenu));
  modalContainer.addEventListener('click', menuClose.bind(modalContainer, mobMenu, menuBtn));


}());