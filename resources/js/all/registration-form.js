
(function() {
    "use strict";

    let regForm = new Form('registrationForm');


    regForm.init(
        function() {
            let name = this.form.name.value;
            let email = this.form.email.value;
            let password = this.form.password.value;
            let repeatPass = this.form.password_confirmation.value;
            let errorMessage = null;
            let errorMsg = document.getElementById("errorMsg");
            let pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if(!name || name.length < 3 || this.isEmptyStr(name)) {
            	this.form.name.classList.add("empty");
            	errorMessage = "Укажите ваше имя.";
            	this.message(errorMessage, errorMsg);
            	return;
            } else {
                this.form.name.classList.remove("empty");}

            if (!email || pattern.test(email) === false) {
                this.form.email.classList.add("empty");
                errorMessage = "Пожалуйста, укажите действительный e`mail.";
                this.message(errorMessage, errorMsg);
                return;
            } else {this.form.email.classList.remove("empty");}
            if (password !== repeatPass) {
                this.form.password.classList.add("empty");
                this.form.password_confirmation.classList.add("empty");
                errorMessage = "Пароли не совпадают!";
                this.message(errorMessage, errorMsg);
                return;
            } else {this.form.password.classList.remove("empty");}


            let formData = {
                name: name,
                email: email,
                password: password

            };
            this.success(formData);
            errorMessage = null;
            console.log(formData);
            // window.location = ;
            this.form.reset();
            return formData;
        },

        function(formData) {
            axios.post('/register', {
                name: formData.name,
                email: formData.email,
                password: formData.password
            }).then( response => {
                    console.log(response.data);
                    this.message("Вы успешно зарегистрированы!", errorMsg);
                })
                .catch( error => {
                    this.message("К сожалению, регистрация не удалась.", errorMsg);
                })
        },
        function() {
            this.message("Ошибка", subscribeMsg);
        }
    );
}());
