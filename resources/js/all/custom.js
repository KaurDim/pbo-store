(function() {
    let accountBtn = document.querySelector('div.account-btn');
    const wrapper = document.querySelector('div.account-menu-wrapper');

    if(accountBtn){
        accountBtn.addEventListener('click', function () {
            wrapper.classList.toggle('hide');
        })
    }
    if(wrapper) {
        wrapper.addEventListener('click', function (event) {
            event.stopPropagation();
            this.classList.remove('hide');
        })
    }
}());