"use strict";

const formSubscribe = new Form("formSubscribe");

formSubscribe.init(
  function() {
    let name = this.form.username.value;
	let email = this.form.email.value;
	let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
	let errorMesage = null;
	let subscribeMsg = document.getElementById("subscribeMsg");
	let pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if(!name || name.length < 3 || this.isEmptyStr(name)) {
	  this.form.username.classList.add("empty");
	  errorMesage = "Укажите ваше имя.";
	  this.message(errorMesage, subscribeMsg);
	  return;
	} else {this.form.username.classList.remove("empty");}
	
	if (!email || pattern.test(email)  == false) {
	  this.form.email.classList.add("empty");
	  errorMesage = "Пожалуйста, укажите действительный email.";
	  this.message(errorMesage, subscribeMsg);
	  return;} else {this.form.email.classList.remove("empty");}


	let formData = {
	  'name' : name,
	  'email': email,

	};

	this.success(formData, token);
	errorMesage = null;


	this.form.reset();
	return formData;
},

    function(formData, token) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', '/subscriptions');
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.send(JSON.stringify(formData));
        xhr.onload = (oEvent) => {
            if (xhr.status == 200) {
                this.message("Благодарим за подписку!\n" +
                    "Ваш ждет интересная и полезная информация.", subscribeMsg);
            } else {

                this.message("К сожалению, подписаться не удлось.", subscribeMsg);
            }
        };

    },

function() {
  this.message("Ошибка", subscribeMsg);

	// this.enabled = true;
}






);