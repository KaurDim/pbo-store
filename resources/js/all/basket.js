
class ModelBasket {
  constructor(modalId, basket, closeBtn) {
    this.openBtn = document.querySelectorAll('[data-openModal="true"]');
    this.closeBtn = document.getElementById(closeBtn);
    this.modal = document.getElementById(modalId);
    this.basket = document.getElementById(basket);
    this.addProduct = document.getElementById('addToCart');
  }
  openBasket() {
    for (let i = 0; i < this.openBtn.length; i++) {
      this.openBtn[i].addEventListener('click', function () {
        this.modal.style.display = 'block';
        setTimeout(function () {
          this.basket.classList.add('move');
          this.closeBtn.classList.add('rotate');
        }, 100);
      }.bind(this))
    }
  }
  closeBasket() {
    document.addEventListener('click', function (event) {
      if (event.target == this.modal) {
        this.basket.classList.remove('move');
        this.closeBtn.classList.remove('rotate');
        setTimeout(function () {
          this.modal.style.display = "none";
        }.bind(this), 400);
      }
    }.bind(this));

    this.closeBtn.addEventListener('click', function () {
      this.basket.classList.remove('move');
      this.closeBtn.classList.remove('rotate');
      setTimeout(function () {
        this.modal.style.display = "none";
      }.bind(this), 400);
    }.bind(this))
  }
  addProductToCart() {
    let storageCart = JSON.parse(localStorage.getItem('cart'));
    if (storageCart) {
      let basketBody = document.querySelector('div.basket-body');
      let producdCard = document.createElement('div').classList.add('product-mini-card');

    }
  }

  init() {

    this.openBasket();
    this.closeBasket();

  }

}
export const Modal = new ModelBasket("basketModal", "basket", "closeBtn");

Modal.init();