$(document).ready(function(){

  $('.main-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    fade: true,
    autoplay: true,
    cssEase: 'linear',
    autoplaySpeed: 4000,
    speed: 800,
    appendDots: $(".dots-container"),
    cssEase: 'linear'
  });


  $('.karusel-slider').slick({
    infinite: true,
    slidesToShow: 3,
    autoplay: true,
    slidesToScroll: 3,
    prevArrow: '<button type="button" class="prewBtn"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="nextBtn"><i class="fas fa-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
    }
    ]

  });

});
	
