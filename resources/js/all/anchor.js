
const  ringsLinks = document.querySelectorAll('.ring-tooltip');
const  companyNavs = document.querySelectorAll('.nav-plate');
const  companyAsideNavs = document.querySelectorAll('.company-aside-link');
const collectionsNavs = document.querySelectorAll('.collection-plate');
const stoneNav = document.querySelectorAll('.scheme-block');

for (let i = 0; i < ringsLinks.length; i++) {
    ringsLinks[i].addEventListener('click', scrollToElem)
}

for (let i = 0; i < companyNavs.length; i++) {
    companyNavs[i].addEventListener('click', scrollToElem)
}

for (let i = 0; i < companyAsideNavs.length; i++) {
    companyAsideNavs[i].addEventListener('click', scrollToElem)
}

for (let i = 0; i < collectionsNavs.length; i++) {
    collectionsNavs[i].addEventListener('click', scrollToElem)
}

for (let i = 0; i < stoneNav.length; i++) {
    stoneNav[i].addEventListener('click', scrollToElem)
}

function scrollToElem (e) {
    e.preventDefault();
    const elemClass = this.getAttribute('data-link');
    document.querySelector(`.${elemClass}`).scrollIntoView();
}
const mainLinks = document.querySelectorAll('.main-menu-link');
