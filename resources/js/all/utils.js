
export function addSpace(price) {
    const str = price.toString();
    const length = str.length;
    const rep = str[length - 4];
    return str.replace(rep, rep + ' ');
}