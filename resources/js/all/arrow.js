'use strict'
let arrow = document.getElementById('box');

window.addEventListener('scroll', function () {
    let scroll = window.pageYOffset;
    if (scroll < 500) {
        arrow.style.display = 'none';
   } else {
        arrow.style.display = 'flex';
    }
});


arrow.addEventListener('click', function () {
   window.scrollTo(0, 0);
});