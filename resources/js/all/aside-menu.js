function scrollMenu(selectorLink) {
    let links = document.querySelectorAll(selectorLink);
    let count = links.length;
    for(let i = 0; i < count; i++) {
        const elemClass = links[i].getAttribute('data-link');
        const elem = document.querySelector(`.${elemClass}`);
        if(elem.offsetTop <= (window.pageYOffset ) &&
            elem.offsetTop + elem.scrollHeight > window.pageYOffset) {
            links[i].classList.add('visible');
        }  else { links[i].classList.remove('visible');}
    }
};

window.addEventListener('scroll', scrollMenu.bind(this,'.company-aside-link'));
