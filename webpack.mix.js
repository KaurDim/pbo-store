const mix = require('laravel-mix');
mix.disableNotifications();
mix.options({ purifyCss: true });
mix.browserSync('http://127.0.0.1:8000');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//Админ
mix.scripts(['resources/js/admin/datatables.js'], 'public/js/admin/admin.js');

mix.styles(['resources/css/admin/template.css'], 'public/css/admin.css');
//все стили
mix.styles(['resources/css/main.css', 'resources/css/normalize.css', 'resources/css/slick.css',
   'resources/css/slick-theme.css', 'resources/css/home-page.css', 'resources/css/contact.css',
   'resources/css/registration.css', 'resources/css/product-page.css', 'resources/css/card-product.css',
   'resources/css/mobile-menu.css', 'resources/css/making.css', 'resources/css/personal-account.css',
   'resources/css/shop.css', 'resources/css/making.css', 'resources/css/istoriya.css',
   'resources/css/aside-menu-active.css',  'resources/css/about_stones.css', 'resources/css/collection-main.css',
   'resources/css/basket.css', 'resources/css/aboutUs.css', 'resources/css/collections-slider.css',
   'resources/css/partnership.css', 'resources/css/vanilla-zoom.css', 'resources/css/basket-style.css',
    'resources/css/range.css', 'resources/css/footer.css', 'resources/css/target-slider.css',
  'resources/css/new-order.css'

], 'public/css/main.css');

mix.sass('resources/sass/main.scss', 'public/css/all.css').options({
    processCssUrls: false
});

mix.scripts([
    'resources/js/all/form.js',
    'resources/js/all/subscribe-form.js',
    'resources/js/all/contact-form.js',
    'resources/js/all/registration-form.js',
    'resources/js/all/login-form.js',
    'resources/js/all/jquery.maskedinput.min.js',
    'resources/js/all/slick.min.js',
    'resources/js/all/slider.js',
    'resources/js/all/basket.js',
    'resources/js/all/mobile-menu.js',
    'resources/js/all/modal.js',
    'resources/js/all/anchor.js',
    'resources/js/all/collection-slider.js',
    'resources/js/all/aside-menu.js',
    'resources/js/all/custom.js',
    'resources/js/all/arrow.js',
    'resources/js/all/images.js',
], 'public/js/all/all.js');

mix.js('resources/js/app.js', 'public/js');
mix.js('resources/js/scripts/index.js', 'public/js');
