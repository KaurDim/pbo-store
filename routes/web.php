<?php

use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $req) {
    $mainSlider = DB::table('slides')
        ->select()->orderBy('slide_number', 'asc')->get();
    $productsOnSlider = DB::table('products_on_slider')->get();
    $products = Product::all();
    $productSlider = [];
    foreach ($products as $product) {
        foreach ($productsOnSlider as $slide) {
            if ($product->id == $slide->product_id) {
                array_push($productSlider, $product);
            }
        }
    }
    return view('home', [
        'title' => 'Per Borup Design',
        'slider' => $mainSlider,
        'productsSlider' => $productSlider
    ]);
})->name('home');

Route::group(['prefix' => 'company', 'namespace'], function () {
    Route::get('about_us', function () {
        return view('company/about_us', ['title' => 'О нас']);
    });
    Route::get('history', function () {
        return view('company/history', ['title' => 'История бренда']);
    });
    Route::get('about_products', function () {
        return view('company/about_products', ['title' => 'Об изделиях']);
    });
    Route::get('production', function () {
        return view('company/production', ['title' => 'Изготовление']);
    });
    Route::get('partnership', function () {
        return view('company/partnership', ['title' => 'Сотрудничество']);
    });
    Route::get('contact_us', function () {
        return view('company/contact_us', ['title' => 'Связаться с нами']);
    });
});

Route::get('catalog', function () {
    return view('company/catalog', ['title' => 'Каталог']);
});

Route::get('instructions', function () {
    return view('company/instructions', ['title' => 'Полезные инструкциии']);
});

Route::get('shipping', function () {
    return view('company/shipping_returns', ['title' => 'Условие покупки, доставки и возврата']);
});
Route::get('store-policy', function () {
    return view('company/store_policy', ['title' => 'Политика конфиденциальности']);
});

Route::get('collections', function () {
    return view('collections/collections', ['title' => 'Коллекции']);
});

Route::get('stones', function () {
    return view('stones/stones', ['title' => 'Сменные камни']);
});

Route::get('podarki', function () {
    return view('target/target', ['title' => 'Повод']);
});

Route::group(['prefix' => 'store'], function () {
    Route::resource('/', 'StoreController');
    Route::post('/', 'StoreController@store');
    Route::get('product/{article}', 'StoreController@getProduct');
    Route::get('cart', 'CartController@cart');
    Route::get('cart/stone', 'CartController@cartStone');
    Route::get('order-success/{number}', 'OrderController@orderSuccess');
});

Route::group(['prefix' => 'account', 'middleware' => 'auth'], function () {
    Route::get('/my-account', 'AccountController@index');
    Route::get('/my-orders', 'AccountController@orders');
    Route::post('/avatar', 'AccountController@changeAvatar');
    Route::post('/update', 'AccountController@updateInfo');
});

Route::get('login', function () {
    abort(404);
});
Route::get('register', function () {
    abort(404);
});

Route::get('registration', function () {
    return view('registration', ['title' => 'Регистрация']);
});
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('logout', 'Auth\LoginController@logout');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::group([
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('reset-password/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});


Route::get('login/{provider}', 'AuthController@redirectToProvider');
Route::get('registration/{provider}', 'AuthController@handleProviderCallback');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['admin', 'auth']], function () {
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::get('/delete/product/{id}', 'ProductsController@deletProduct');
    Route::post('/order/products', 'ProductsController@changeOrder');
    Route::resource('/products', 'ProductsController');
    Route::resource('/collections', 'CollectionsController');
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/images', 'ImagesController');
    Route::resource('/users', 'UsersController');
    Route::resource('/orders', 'OrdersController');
    Route::resource('/targets', 'TargetsController');
    Route::resource('/materials', 'MaterialsController');
    Route::resource('/material-types', 'MaterialTypesController');
    Route::resource('/insertions', 'InsertionsController');
    Route::resource('/slider', 'SliderController');
    Route::get('/change-visible/{id}', 'ProductsController@changeVisible');
    Route::get('/deleteImage/{id}', 'ProductsController@deleteProductImage');
    Route::get('/add-to-slider/{id}', 'ProductsController@addToSlider');
    Route::post('/orders/complete', 'OrdersController@completeOrder');
    Route::post('/orders/payments', 'OrdersController@changePayments');
    Route::post('/orders/status', 'OrdersController@changeStatus');
});

Route::post('/subscriptions', 'CustomerController@subscribe');


Route::post('/add-to-cart', 'CartController@addToCart');
Route::get('/remove-from-cart/{tag}/{id}', 'CartController@removeFromCart');
Route::get('/get-cart-content', 'CartController@getCartContent');
Route::post('/update-cart', 'CartController@update');

Route::post('/feedback', 'CustomerController@sendMessage');
Route::post('/order', 'OrderController@makeOrder');
Route::get('/order', function () {
    abort(404);
});
