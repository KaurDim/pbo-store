<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products', function (Blueprint $table) {
      $table->increments('id');
      $table->string('vendor')->unique()->nullable();
      $table->string('name')->nullable();
      $table->string('category_id');
      $table->decimal('price', 10, 2);
      $table->decimal('max_price', 10, 2)->nullable();
      $table->boolean('in_stock')->default(1);
      $table->text('description')->nullable();
      $table->text('meta_description')->nullable();
      $table->string('meta_keywords')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('products');
  }
}
