<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionsSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('colection_name');
            $table->integer('collection_id');
            $table->mediumText('description');
            $table->string('button_text');
            $table->text('short_description');
            $table->json('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections_slider');
    }
}
