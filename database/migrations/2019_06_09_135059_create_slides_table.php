<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('slides', function (Blueprint $table) {
      $table->increments('id');
      $table->string('header', 255);
      $table->integer('slide_number')->unique();
      $table->mediumText('content');
      $table->string('image', 255);
      $table->string('button_text', 255);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('slides');
  }
}
