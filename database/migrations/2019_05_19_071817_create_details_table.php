<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('details', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('product_id');
      $table->integer('order_id');
      $table->string('stones_id')->nullable();
      $table->string('size')->nullable();
      $table->string('material');
      $table->string('gold');
      $table->integer('quantity');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('details');
  }
}
